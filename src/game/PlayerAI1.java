package game;

import java.util.Stack;

import board.Board;
import board.Disk;
import board.Field;
import board.Field.Direction;

/**
*
* @author Milos Dolinsky, Pavel Mencner
* Reprezentuje hrace ovladaneho umelou inteligenci.
* 
*/
public class PlayerAI1 implements Player {
	private int startPosX, startPosY;
	private boolean isWhite;
	private Stack<Disk> pool;
	private String name;
	private Board board;
	private boolean isPerson;
	
	/**
     * Inicializace hrace.
     * @param isWhite - Urcuje barvu hrace (true = bily, false = cerny) 
     * @param name - Jmeno
     * @param board - Hraci deska.
     * @param size - Velikost hraci desky.
     */
	public PlayerAI1(boolean isWhite, String name, Board board, int size) {
		this.isWhite = isWhite;
		this.name = name;
		this.board = board;
		this.startPosX = size / 2;
		this.startPosY = (size / 2) - 1;
		this.isPerson = false;
	}
	
	/**
	 * Vraci typ hrace (PC, osoba).
	 * @return - Jestli je hrac clovek, vrati true. Jinak false.
	 */
	@Override
	public boolean isPerson() {
		return this.isPerson;
	}
	
	/**
	 * Vrati typ hrace
	 * @return - Vraci typ hrace.
	 */
	public String getPlayerType() {
		return "PC type 1";
	}
	
	/**
	 * Provede tah za hrace ovladaneho pocitacem. Vybere vhodne pole na umisteni kamene, podle toho, kde by se otocilo nejvice
	 * souperovych kamenu.
	 * @param board - Hraci deska.
	 * @return - True v pripade, ze je mozno provest tah. False v pripade, ze to mozno neni.
	 */
	public boolean makeMove(Board board) {
		Direction dir = Direction.D; // Inicializace na pocatecni smer.
		int step = 2; // Pocatecni krok.
		Disk disk;
		Field field = board.getField(this.startPosX, this.startPosY); // Pocatecni pozice.
		
		int maxCnt = 0, cnt;
		Field chosenOne = null;		
		
		/*
		 * Spiralovity pruchod hraci deskou zevnitr ven. Pokud narazi na prazdne pole, podiva se do jeho okoli, jestli 
		 * tam neni sekvence souperovych kamenu koncici hracovym kamenem. V teto sekvenci spocita pocet souperovych
		 * kamenu, ktere je mozno umistenim kamene na toto volne misto otocit (pokud to lze). Hracuv kamen bude ulozen na pole,
		 * ktere by otocilo nejvetsi mnozstvi souperovych kamenu.
		 */
		while (field != null) {
			for (int i = 0; i < step; i++) {
				disk = field.getDisk(); // Ulozi disk z aktualniho pole.
				
				if (disk == null) {
					// Koukne do okoli, pocita mnozstvi N potencionalnich otocenych kamenu, ulozi si N, policko.
					cnt = 0;
					for (Direction dirs : Direction.values())
						cnt += getNextDisk(dirs, field, this.board.getRules().isFreezing());
					
					if (cnt > maxCnt) {
						maxCnt = cnt;
						chosenOne = field;
					}
				}
				
				if ((field = field.nextField(dir, this.board)) == null) // Presun na dalsi pole ve smeru.
					break; // Zarazka, aby se nepokracovalo dal.
			}
				
			// Nastavi smer pro dalsi kolo.
			switch (dir) {
				case D:
					step++;
					dir = Direction.R;
					break;
				case R:				
					dir = Direction.U;
					break;
				case U:
					step++;
					dir = Direction.L;
					break;
				case L:
					dir = Direction.D;
					break;
				default:
					break;
			}
			
		}
		
		if (chosenOne == null)
			return false; // Tah nebylo mozne uskutecnit.
		else {
			// tah
			putDisk(chosenOne);
			return true;
		}
	}
	
	/**
     * V urcitem smeru spocita pocet souperovych kamenu.
     * @param dir - Smer, kterym se pokracuje.
     * @param field - Aktualni pole.
     * @param isFreezing - Jestli je povoleno zamrzani kamenu.
     * @return - Pocet souperovych kamenu nez narazi na kamen hracovy barvy. V opacnem pripade 0.
     */
	@Override
	public int getNextDisk(Direction dir, Field field, boolean isFreezing) {
		int count = 0;
		Field nextField = field;	
		Disk disk;
		
		while (true) {
			nextField = nextField.nextField(dir, this.board);
			
			if (nextField == null) // Zda pole neni okrajove.
				return 0;
			
			disk = nextField.getDisk(); // Disk na tomto poli.
			
			if (disk == null) // Zda neni pole prazdne.
				return 0; // V tomto smeru neni mozne otocit zadne kameny.
			else if (isFreezing && disk.isFreezed())
				return 0;
			else if (this.isWhite != disk.isWhite())
				count++;
			else if (this.isWhite == disk.isWhite())
				return count;
			else
				return 0;
		}
	}

	/**
     * Test barvy hrace.
     * @return - Zda je hrac bily.
     */
	@Override
	public boolean isWhite() {
		return this.isWhite;
	}

	/**
	 * Obrati vsechny kameny v urcitem smeru dokud nenarazi na kamen hracovy barvy.
	 * Predtim musi byt osetreno, ze se v tomto smeru skutecne hracuv kamen nachazi.
	 * @param dir - Smer.
	 * @param field - Aktualni pole.
	 * @return - True v pripade uspechu.
	 */	
	@Override
	public boolean flipAllDisks(Direction dir, Field field) {
        Field nextField = field.nextField(dir, this.board);
        Disk nextDisk = nextField.getDisk();
        
        if (this.isWhite == nextDisk.isWhite())
            return true;
        else if (this.isWhite != nextDisk.isWhite()) {
        	nextField.getDisk().turn();
            return flipAllDisks(dir, nextField);
        }   
        else
            return false;
	}
	
	/**
     * Vraci jmeno hrace.
     * @return - Vrati jmeno hrace.
     */
    @Override
    public String getName() {
    	return this.name;
    }

	/**
     * Test, zda je mozne vlozit novy kamen hrace na dane pole.
     * Kamen ze sady nevybira ani nevklada na pole.
     * @param field - Pole, na ktere se ma vkladat kamen.
     * @return - Zda je mozne kamen vlozit.
     */
	@Override
	public boolean canPutDisk(Field field) {
        Field nextField = field;
        boolean canPut = false;
        
        if (field.getDisk() == null) {
            for (Direction dir : Direction.values()) {
            	if (getNextDisk(dir, nextField, false) > 0) {
                	canPut = true;
                }
            }
        }
        return canPut;
	}
	
	/**
     * Test, zda je mozne vlozit novy kamen hrace na dane pole v pripade, ze je aktivovano zamrzani kamenu.
     * Kamen ze sady nevybira ani nevklada na pole.
     * @param field - Pole, na ktere se ma vkladat kamen.
     * @return - Zda je mozne kamen vlozit.
     */
    @Override
	public boolean canPutDiskFreeze(Field field) {
        Field nextField = field;
        boolean canPut = false;
        
        if (field.getDisk() == null) {
            for (Direction dir : Direction.values()) {
            	if (getNextDisk(dir, nextField, true) > 0) {
                	canPut = true;
                }
            }
        }
        return canPut;
    }

	/**
     * Test prazdnosti sady kamenu, ktere ma hrac k dispozici.
     * @return - Zda je sada prazdna.
     */
	@Override
	public boolean emptyPool() {
		if (this.pool == null || this.pool.empty())
            return true;
        else
            return false;
	}

	/**
     * Vlozi novy kamen hrace na dane pole, pokud to pravidla umoznuji.
     * Pokud lze vlozit, vybere kamen ze sady a vlozi na pole.
     * @param field - Pole, na ktere se vklada kamen.
     * @return - Uspech akce.
     */
	@Override
	public boolean putDisk(Field field) {
        Disk putDisk;
        
        if (this.board.getRules().isFreezing()) {
        	if (!canPutDiskFreeze(field))
                return false; // Pravidla neumoznuji vlozit kamen.
        }
        else {
        	if (!canPutDisk(field))
                return false; // Pravidla neumoznuji vlozit kamen.
        }
        
        if (!this.emptyPool()) {
            Field nextField = field;
        
            if (field.getDisk() == null) {
                for (Direction dir : Direction.values()) {
                	if (getNextDisk(dir, nextField, false) > 0) {
                        // Obrat kameny v tomto smeru.
                        flipAllDisks(dir, nextField);
                    }
                }
            }
            putDisk = (Disk)this.pool.pop();        
            return field.putDisk(putDisk);   
        }
        else
            return false;
	}

	/**
     * Inicializace hrace v ramci hraci desky.
     * Vytvori sadu kamenu o prislusne velikosti a umisti sve pocatecni kameny na desku.
     * @param board - Deska, v ramci ktere se hrac inicializuje.
     */
	@Override
	public void init(Board board) {
	       int poolSize = board.getSize() * board.getSize();
	        this.pool = new Stack<Disk>();
	        
	        for (int i = 0; i < poolSize; i++) {
	            this.pool.push(new Disk(this.isWhite));
	        }
	        
	        int center = board.getSize() / 2;
	        Disk disk; // Vkladany disk.
	        Field field; // Policko, na ktere se vklada disk.
	        boolean putDisk; // Pro ulozeni navratove hodnoty pri volani metody putDisk();
	        
	        
	        if (this.isWhite) {
	            field = board.getField(center, center);
	            disk = (Disk)this.pool.pop();
	            putDisk = field.putDisk(disk);
	            if (!putDisk) System.out.println("Nepodarilo se vlozit kamen.");
	            
	            field = board.getField(center+1, center+1);          
	            disk = (Disk)this.pool.pop();
	            putDisk = field.putDisk(disk);
	            if (!putDisk) System.out.println("Nepodarilo se vlozit kamen.");
	        }
	        else {
	            field = board.getField(center+1, center);
	            disk = (Disk)this.pool.pop();
	            putDisk = field.putDisk(disk);
	            if (!putDisk) System.out.println("Nepodarilo se vlozit kamen.");
	            
	            field = board.getField(center, center+1);          
	            disk = (Disk)this.pool.pop();
	            putDisk = field.putDisk(disk);
	            if (!putDisk) System.out.println("Nepodarilo se vlozit kamen.");
	        }
		
	}
	
	/**
	 * Dostupny pocet kamenu hrace
	 * @return Pocet kamenu
	 */
	public int getNumDisks() {
		return pool.size();
	}
	
	/**
	 * Nastavi pocet kamenu hrace
	 */
	public void setNumDisks(int numDisks) {
		//Vyhazej stare kameny
		while (pool.size() > 0)
			pool.pop();
		
		//Vytvor nove
		for (int i = 0; i < numDisks; i++) {
            this.pool.push(new Disk(this.isWhite));
        }
	}
	
	/**
	 * Vrati hraci zpet jeden kamen v pripade undo operace 
	 */
	public void undoDisk() {
		this.pool.push(new Disk(this.isWhite));
	}
}

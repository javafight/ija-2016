/*
 * Autori: Milos Dolinsky, xdolin01
 *         Pavel Mencner, xmencn00
 * IJA 2015/2016
 * Trida GameModel reprezentujici aktualni hru.
 */
package game;


import java.util.ArrayList;
import java.util.Collections;

import board.Board;
import board.Disk;
import board.Field;
import board.Rules;

/**
*
* @author Milos Dolinsky, Pavel Mencner
* Trida ReversiRules implementujici rozhrani Rules.
* 
*/
public class GameModel extends java.util.Observable {
  
	private Player white;
    private Player black;
    private Board board;
    private Board undoBoard;
    private Rules rules;
    private Player current;
    
    private boolean canUndo;
    private boolean isFreezed;
    private long start, end, time;
    
    /**
     * Zakladni inicializace.
     */
    public GameModel() {
    	canUndo = false;
    	isFreezed = false;
    	start = 0;
    	end = 0;
    	time = 0;
    }
    
    /**
     * Inicializace hraci desky.
     * @param board - Hraci deska.
     */
    private void initBoard(Board board) {
    	this.board = board;
    	this.undoBoard = new Board(board);
    }
    
    /**
     * Inicializace pravidel.
     * @param rules - Pravidla.
     */
    private void initRules(Rules rules) {
    	this.rules = rules;
    }
    
    /**
     * Inicializuje a spusti novou hru, pouziva se i pri rozehrane hre jako restart
     * @param size - Velikost hraci desky.
     * @param playerWhite - Typ hrace 1. Ocekava textovou reprezentaci z pole definovaneho v getPlayerTypes().
     * @param playerBlack - Typ hrace 2. Ocekava textovou reprezentaci z pole definovaneho v getPlayerTypes().
     * @param p1name - Jmeno hrace 1.
     * @param p2name - Jmeno hrace2.
     * @param gaps - Interval, po jehoz uplynuti se zablokuje kamen.
     * @param maxTime - Interval, po kterem se kamen odblokuje.
     * @param numDisks - Pocet disku, ktere se zablokuji.
     */
    public void startNewGame(String size, String playerWhite, String playerBlack, String p1name, String p2name, String gaps, String maxTime, String numDisks) {
    	canUndo = false;
    	int isize = Integer.parseInt(size);
    	
    	initRules(new ReversiRules(isize, Integer.parseInt(gaps), Integer.parseInt(maxTime), Integer.parseInt(numDisks)));
    	initBoard(new Board(this.rules));
    	
    	// Vytvoreni bileho hrace.
    	if (playerWhite.equals("Person")) {
    		addPlayer(new PlayerPerson(true, p1name, this.board));
    	}
    	else if (playerWhite.equals("PC type 1")) {
    		addPlayer(new PlayerAI1(true, p1name, this.board, isize));
    	}
    	else if (playerWhite.equals("PC type 2")) {
    		addPlayer(new PlayerAI2(true, p1name, this.board, isize));
    	}
    	
    	// Vytvoreni cerneho hrace.
    	if (playerBlack.equals("Person")) {
    		addPlayer(new PlayerPerson(false, p2name, this.board));
    	}
    	else if (playerBlack.equals("PC type 1")) {
    		addPlayer(new PlayerAI1(false, p2name, this.board, isize));
    	}
    	else if (playerBlack.equals("PC type 2")) {
    		addPlayer(new PlayerAI2(false, p2name, this.board, isize));
    	}
    	
    	if (this.board.getRules().isFreezing()) {
    		this.time = getTime(this.board.getRules().getGaps());
    		this.start = System.currentTimeMillis();
    	}
    	
    	notifyChange();
    }
    
    /**
     * Overload metody vyse. Nemusi se zadavat volitelne argumenty pro data ohledne zamrzani kamenu.
     * @param size - Velikost hraci desky.
     * @param playerWhite - Typ hrace 1. Ocekava textovou reprezentaci z pole definovaneho v getPlayerTypes().
     * @param playerBlack - Typ hrace 2. Ocekava textovou reprezentaci z pole definovaneho v getPlayerTypes().
     * @param p1name - Jmeno hrace 1.
     * @param p2name - Jmeno hrace 2.
     */
    public void startNewGame(String size, String playerWhite, String playerBlack, String p1name, String p2name) {
    	startNewGame(size, playerWhite, playerBlack, p1name, p2name, "0", "0", "0");
    }
    
    /**
     * Restartuje aktualni rozehranou hru
     */
    public void restart() {
    	if (board == null)
    		return;
    	
    	startNewGame(Integer.toString(board.getSize()), white.getPlayerType(), black.getPlayerType(), white.getName(), black.getName(), Integer.toString(rules.getGaps()),
    			Integer.toString(rules.getMaxTime()), Integer.toString(rules.getNumDisks()));
    	
    	makeTurnPC();
    }
    
    /**
     * Provede tah za pocitac, pokud je pocitac na rade a je ve hre.
     */
    public void makeTurnPC() {
    	// Pokud hrac neni clovek.
    	if (!this.current.isPerson()) {
    		Thread t = new Thread() {
    			public void run() {
    				try {
						sleep(1500);
					} catch (Exception e) {
						e.printStackTrace();
					}
    				checkFreezing();
    				current.makeMove(board);
    				nextPlayer();
    	    		notifyChange();
    			}
    		};
    		t.start();
    	}
    }
    
    /**
     * Provede tah.
     * @param row - Radek, kam se ma kamen vlozit.
     * @param col - Sloupec, kam se ma kamen vlozit.
     */
    public void makeTurn(int row, int col) {
    	this.undoBoard.setBoard(this.board);
    	
    	canUndo = true;
    	
    	
    	
    	
    	//notifyChange();
    	  	
    	this.current.putDisk(this.board.getField(row, col));
    	
    	checkFreezing();
    	
    	/*Field field;
    	Disk disk;
    	for (int y = 1; y <= board.getSize(); y++) {
    		for (int x = 1; x <= board.getSize(); x++) {
    			field = board.getField(x, y);
    			disk = field.getDisk();
    			
    			
    			if (disk == null)
    				continue;
    			else if (disk.isFreezed()) {
    				System.out.println("x: " + x + " y: " + y);
    				System.out.println("Disk: zmrzly");
    			}
    			else if (!disk.isFreezed()) {
    				System.out.println("x: " + x + " y: " + y);
    				System.out.println("Disk: nezmrzly");
    			}
    		}
    	}*/
    	
    	
    	nextPlayer();
    	
    	notifyChange();
    	
    	makeTurnPC(); // Tah za pocitac pokud ve hre pocitac je.
    }
    
    /**
     * Vola se na zacatku kazdeho tahu.
     */
    public void checkFreezing() {
    	if (board.getRules().isFreezing()) { // Je povoleno zamrzani kamenu?
    		end = System.currentTimeMillis();
    		if (((end - start) / 1000) >= time) { // Ubehl casovy limit?
    			start = System.currentTimeMillis();
    			if (this.isFreezed) {
    	    		// Doba zablokovani kamene ubehla.
    	    		time = getTime(this.board.getRules().getGaps()); // Cas nez se zablokuji kameny.
    	    		unfreezeDisks();
    	    		this.isFreezed = false;
    	    	}
    	    	else {
    	    		// Casova mezera mezi zablokovanim ubehla.
    	    		time = getTime(this.board.getRules().getMaxTime()); // Cas zablokovani.
    	    		freezeDisks();
    	    		this.isFreezed = true;
    	    	}		
        	}
    	}
    }
    
    /**
     * Rozmrazi vsechny zmrazene disky.
     */
    public void unfreezeDisks() {
    	Field field;
    	Disk disk;
    	
    	this.isFreezed = false; // Vsechny kameny odmrznou.
    	
    	for (int y = 1; y <= this.board.getSize(); y++) {
    		for (int x = 1; x <= this.board.getSize(); x++) {
    			field = this.board.getField(x, y);
    			
    			if ((disk = field.getDisk()) != null) {
    				if (disk.isFreezed()) 	
    					disk.unfreeze();   				
    			}
    		}
    	}
    }
    
    /**
     * Nahodne vybere a zmrazi urcity pocet kamenu.
     */
    public void freezeDisks() {
    	Field field;
    	Disk disk;
    	int counter = 0;
    	
		ArrayList<Integer> listRow = new ArrayList<Integer>(); // List ocislovanych poli v radku
		ArrayList<Integer> listCol = new ArrayList<Integer>(); // List ocislovanych poli v sloupci
		for (int i = 1; i <= this.board.getSize(); i++) { // Naplneni
			listRow.add(new Integer(i));
			listCol.add(new Integer(i));
		}
		Collections.shuffle(listRow); // Nahodne zamicha pole X souradnic
		Collections.shuffle(listCol); // Nahodne zamicha pole Y souradnic
		
		for (int y = 0; y < this.board.getSize(); y++) {
			for (int x = 0; x < this.board.getSize(); x++) {
				field = this.board.getField(listRow.get(x), listCol.get(y));
				
				if (counter == this.rules.getNumDisks())
					return;
				
				if ((disk = field.getDisk()) != null) {
					disk.freeze();
					counter++;
				}
			}
		}
    }
    
    /**
     * Vrati nahodne cislo z intervalu <0, time>.
     * @param time - Horni mez intervalu.
     * @return - Nahodne cislo z intervalu.
     */
    public int getTime(int time) {
    	ArrayList<Integer> times = new ArrayList<Integer>();
    	for (int i = 1; i <= time; i++)
    		times.add(new Integer(i));
    	Collections.shuffle(times);
    	
    	return times.get(0); // Vrati nahodny cislo.
    }
    
    /**
     * Prida hrace a soucasne vyvola jeho inicializaci, pokud hrac existuje nahradi ho. Nutna prazdna hraci deska
     * @param player - Pridavany hrac.
     * @return - Uspech akce.
     */
	public boolean addPlayer(Player player) {
		boolean newIsWhite = player.isWhite();
		if (newIsWhite) {
			this.white = player;
			this.white.init(this.board);
			this.current = this.white;
		} else {
			this.black = player;
			this.black.init(this.board);
		}
		return true;
	}
    
    /**
     * Vrati aktualniho hrace, ktery je na tahu.
     * @return - Aktualni hrac.
     */
    public Player currentPlayer() {
        return this.current;
    }
    
    /**
     * Zmeni aktualniho hrace.
     * @return - Aktualne nastaveny hrac.
     */
    public Player nextPlayer() {
        if (this.current.isWhite())
            this.current = this.black;
        else
            this.current = this.white;
        
        return this.current;
    }
    
    /**
     * Vrati hraci desku.
     * @return - Hraci deska.
     */
    public Board getBoard() {
        return this.board;
    }
    
    /**
     * Vrati hru o tah zpet
     */
    public void undo() {
    	this.board.setBoard(this.undoBoard);
    	
    	//Prepne hrace, pokud predchozi tah byl PC prepne jeste jednou
    	nextPlayer();
    	current.undoDisk();
    	
    	if (!current.isPerson()) {
    		nextPlayer();
    		current.undoDisk();
    	}
    	
    	canUndo = false;
    	
    	notifyChange();
    }
    
    /**
     * Mozne velikosti hraci desky 
     * @return Pole stringu se vsemi moznostmi 
     */
    public String[] getBoardSizes() {
    	String[] boardSizes = { "6", "8", "10", "12" };
    	return boardSizes;
    }
    
    /**
     * Mozne typy hracu vcetne vsech moznosti AI hracu 
     * @return Pole stringu se vsemi moznostmi 
     */
    public String[] getPlayerTypes() {
    	String[] playerType = { "Person", "PC type 1", "PC type 2" };
    	return playerType;
    }
    
    /**
     * Mozne delky intervalu mezi zamrzanim kamenu  
     * @return Pole stringu se vsemi moznostmi 
     */
    public String[] getFreezeGaps() {
    	String[] boardSizes = { "10", "20", "30", "40", "50", "60", "70", "80", "90", "100", "110", "120" };
    	return boardSizes;
    }
    
    /**
     * Mozne hodnoty doby zamrzani kamenu 
     * @return Pole stringu se vsemi moznostmi 
     */
    public String[] getFreezeMaxTime() {
    	String[] boardSizes = { "5", "10", "15", "20", "25", "30", "35", "40", "45", "50", "55", "60" };
    	return boardSizes;
    }
    
    /**
     * Mozny pocet zamrzajicich kamenu 
     * @return Pole stringu se vsemi moznostmi 
     */
    public String[] getFreezeNumDisks() {
    	String[] boardSizes = { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20" };
    	return boardSizes;
    }
    
    /**
     * Upozorni pozorovatele o zmene dat
     */
    public void notifyChange() {
    	setChanged();
    	notifyObservers();
    }
    
	/**
	 * Vrati serializovany stav hry pro ulozeni do souboru
	 * 
	 * @return serializovana hra
	 */
	public String getSerializedState() {
		String ser = "";
		
		//Info bily hrac
		ser += white.getName() + "\n";
		ser += white.getPlayerType() + "\n";
		ser += Integer.toString(white.getNumDisks()) + "\n";
		
		//Info cerny hrac
		ser += black.getName() + "\n";
		ser += black.getPlayerType() + "\n";
		ser += Integer.toString(black.getNumDisks()) + "\n";
		
		//Kdo je na tahu
		ser += (current.isWhite()? "white": "black") + "\n";
		
		//Velikost desky
		ser += Integer.toString(board.getSize()) + "\n";
		
		//Zamrzani kamenu
		ser += (board.getRules().isFreezing()? "1": "0") + "\n";
		ser += Integer.toString(board.getRules().getGaps()) + "\n";
		ser += Integer.toString(board.getRules().getMaxTime()) + "\n";
		ser += Integer.toString(board.getRules().getNumDisks()) + "\n";
		
		//Stav hraci desky (Druh kamene + jestli je zamrzly
		for (int row = 0; row < board.getSize(); row++) {
			for (int col = 0; col < board.getSize(); col++) {

				Disk disk = board.getField(row+1, col+1).getDisk();
				
				if (disk == null) {
					ser += "00\n";
					continue;
				}
				
				if (disk.isWhite())
					ser += "1";
				else
					ser += "2";
				
				if (disk.isFreezed())
					ser += "1\n";
				else
					ser += "0\n";
			}
		}

		return ser;
	}
    
    /**
     * Nastavi model ze serializovanych dat, nacteni hry ze souboru
     * @return true pokud uspesne nastaven stav
     */
    public boolean setSerializedState(String state) {
    	
    	String lines[] = state.split("\\n");
    	
    	int cnt = 0;
    	
    	String name1, type1, name2, type2, onTurn;
    	int numDisks1, numDisks2, boardSize, freeze, fGaps, fTime, fDisks;
    	
    	if (lines.length < 12) //Pocet radku bez informaci o polich
    		return false;
    	
    	name1 = lines[cnt++];
    	type1 = lines[cnt++];
    	
    	if (!type1.equals("Person") && !type1.equals("PC type 1") && !type1.equals("PC type 2"))
    		return false;
    	
    	try {
    		numDisks1 = Integer.parseInt(lines[cnt++]);
    	} catch (NumberFormatException e) {
    		return false;
    	}
    	
    	name2 = lines[cnt++];
    	type2 = lines[cnt++];
    	
    	if (!type2.equals("Person") && !type2.equals("PC type 1") && !type2.equals("PC type 2"))
    		return false;
    	
    	try {
    		numDisks2 = Integer.parseInt(lines[cnt++]);
    	} catch (NumberFormatException e) {
    		return false;
    	}
    	
    	if (!type1.equals("Person") && !type2.equals("Person"))
    		return false;
    	
    	onTurn = lines[cnt++];
    	
    	if (!onTurn.equals("black") && !onTurn.equals("white"))
    		return false;
    	
    	try {
    		boardSize = Integer.parseInt(lines[cnt++]);
    	} catch (NumberFormatException e) {
    		return false;
    	}
    	
    	if (boardSize != 6 && boardSize != 8 && boardSize != 10 && boardSize != 12)
    		return false;
    	
    	try {
    		freeze = Integer.parseInt(lines[cnt++]);
    	} catch (NumberFormatException e) {
    		return false;
    	}
    	
    	if (freeze != 0 && freeze != 1)
    		return false;
    	
    	try {
    		fGaps = Integer.parseInt(lines[cnt++]);
    	} catch (NumberFormatException e) {
    		return false;
    	}
    	
    	if (fGaps < 0 || fGaps > 200)
    		return false;
    	
    	try {
    		fTime = Integer.parseInt(lines[cnt++]);
    	} catch (NumberFormatException e) {
    		return false;
    	}
    	
    	if (fTime < 0 || fTime > 200)
    		return false;
    	
    	try {
    		fDisks = Integer.parseInt(lines[cnt++]);
    	} catch (NumberFormatException e) {
    		return false;
    	}
    	
    	if (fDisks < 0 || fDisks > 20)
    		return false;
    	
    	//Informace o hracich a nastaveni hry je validni
    	
    	//Kontrola validity hraci desky
    	
    	if (cnt + boardSize*boardSize != lines.length) //Zda ma soubor spravny pocet radku 
    		return false;
    	
    	Board b = new Board(new ReversiRules(boardSize, fGaps, fTime, fDisks));
    	
		for (int row = 0; row < boardSize; row++) {
			for (int col = 0; col < boardSize; col++) {
				String line = lines[cnt + row*boardSize + col];
				
				if (line.equals("00"))
					continue;
				
				if (line.equals("10")) {
					b.getField(row+1, col+1).putDisk(new Disk(true));
					continue;
				}
				
				if (line.equals("11")) {
					Disk d = new Disk(true);
					d.freeze();
					b.getField(row+1, col+1).putDisk(d);
					continue;
				}
				
				if (line.equals("20")) {
					b.getField(row+1, col+1).putDisk(new Disk(false));
					continue;
				}
				
				if (line.equals("21")) {
					Disk d = new Disk(false);
					d.freeze();
					b.getField(row+1, col+1).putDisk(d);
					continue;
				}
				
				return false;
			}
		}
		
		//Deska je validni a inicializovana
		
		//Start nove hry
		startNewGame(Integer.toString(boardSize), type1, type2, name1, name2, Integer.toString(fGaps), Integer.toString(fTime), Integer.toString(fDisks));
		
		//Prepsani currentPlayer a board podle save
		if (onTurn.equals("white"))
			current = white;
		else
			current = black;
		
		white.setNumDisks(numDisks1);
		black.setNumDisks(numDisks2);
		
		board.setBoard(b);
		
		notifyChange();
		
		makeTurnPC();
		
    	return true;
    }
    
    public boolean canUndo() {
    	return canUndo;
    }
    
    /**
     * Aktualni pocet kamenu daneho hrace
     * @param isWhite ktery hrac
     * @return pocet kamenu na desce
     */
	public int getScore(boolean isWhite) {
		int boardSize = board.getSize();
		int white = 0;
		int black = 0;
		
		if (board == null)
			return 0;
		
		for (int row = 1; row <= boardSize; row++) {
			for (int col = 1; col <= boardSize; col++) {
				if (board.getField(row, col).getDisk() == null)
					continue;
				
				if (board.getField(row, col).getDisk().isWhite())
					white++;
				else
					black++;
			}
		}
		
		if(isWhite)
			return white;
		else
			return black;
	}
	
	/**
	 * Zda muze provest aktualni hrac tah
	 * @return true pokud muze tahnout
	 */
	public boolean isPossibleTurn() {
		int boardSize = board.getSize();
		
		if (board == null)
			return false;
		
		for (int row = 1; row <= boardSize; row++) {
			for (int col = 1; col <= boardSize; col++) {
				if (current.canPutDiskFreeze(board.getField(row, col)))
					return true;
			}
		}
		
		return false;
	}
	
	/**
	 * Zda hra skoncila
	 * @return true pokud hra uz skoncila
	 */
	public boolean gameEnd() {
		int boardSize = board.getSize();
		
		if (board == null)
			return true;
		
		for (int row = 1; row <= boardSize; row++) {
			for (int col = 1; col <= boardSize; col++) {
				if ( (!white.emptyPool() && white.canPutDisk(board.getField(row, col))) || (!black.emptyPool() && black.canPutDisk(board.getField(row, col))) )
					return false;
			}
		}
		
		return true;
	}
	
	public Player getPlayer(boolean isWhite) {
		if (isWhite)
			return white;
		else
			return black;
	}
	
	public boolean passTurn() {
		if (isPossibleTurn())
			return false;
		
		canUndo = false;
		checkFreezing();
		nextPlayer();
		notifyChange();
		makeTurnPC();
		return true;
	}
}

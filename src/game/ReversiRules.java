/*
 * Autori: Milos Dolinsky, xdolin01
 *         Pavel Mencner, xmencn00
 * IJA 2015/2016
 * Trida ReversiRules implementujici rozhrani Rules.
 */
package game;

import board.Field;
import board.Rules;
import board.BoardField;
/**
 *
 * @author Milos Dolinsky, Pavel Mencner
 * Trida ReversiRules implementujici rozhrani Rules.
 * 
 */
public class ReversiRules implements Rules {
    private int size;
    private int numberDisks;
    private int freezeGaps;
    private int freezeMaxTime;
    private int freezeNumDisks;
    private boolean isFreezing;
    
    /**
     * Inicializace pravidel.
     * @param size - Velikost hraci desky.
     */
    public ReversiRules(int size, int gaps, int maxTime, int numDisks) {
        this.size = size;
        this.numberDisks = size*size;
        this.freezeGaps = gaps;
        this.freezeMaxTime = maxTime;
        this.freezeNumDisks = numDisks;
        
        if (gaps > 0)
        	this.isFreezing = true;
        else
        	this.isFreezing = false;
    }
    
    /**
     * Vraci velikost desky.
     * @return - Velikost desky.
     */
    @Override
    public int getSize() {
        return this.size;
    }
    
    /**
     * Vraci jestli je aktivovano zamrzani kamenu.
     * @return - True jestli je aktivovano. Jinak false.
     */
    @Override
    public boolean isFreezing() {
    	return this.isFreezing;
    }
    
    /**
     * Vrati cas mezi zamrznutim kamenu.
     * @return - Vrati cas mezi zamrznutim kamenu
     */
    @Override
    public int getGaps() {
    	return this.freezeGaps;
    }
    
    /**
     * @return - Vrati maximalni dobu zamrznuti kamene.
     */
    @Override
    public int getMaxTime() {
    	return this.freezeMaxTime;
    }
    
    /**
     * @return - Vrati pocet zamrznutych disku.
     */
    @Override
    public int getNumDisks() {
    	return this.freezeNumDisks;
    }
    
    /**
     * Vraci pocet kamenu jednotlivych hracu.
     * @return - Pocet kamenu.
     */
    @Override
    public int numberDisks() {
        return this.numberDisks;
    }
    
    /**
     * Vytvori odpovidajici pole na zadanych indexech.
     * @param row - Radek desky.
     * @param col - Sloupec desky.
     * @return - Vytvorene pole.
     */
    @Override
    public Field createField(int row, int col) {
        return new BoardField(row, col);
    }
}

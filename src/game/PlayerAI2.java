package game;

import board.Board;
import board.Disk;
import board.Field;
import board.Field.Direction;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Stack;

/**
*
* @author Milos Dolinsky, Pavel Mencner
* Reprezentuje hrace ovladaneho umelou inteligenci.
* 
*/
public class PlayerAI2 implements Player {	
	private int size;
	private boolean isWhite;
	private Stack<Disk> pool;
	private String name;
	private Board board;
	private boolean isPerson;
	
	/**
     * Inicializace hrace.
     * @param isWhite - Urcuje barvu hrace (true = bily, false = cerny) 
     * @param name - Jmeno
     * @param board - Hraci deska.
     * @param size - Velikost hraci desky.
     */
	public PlayerAI2(boolean isWhite, String name, Board board, int size) {
		this.isWhite = isWhite;
		this.name = name;
		this.board = board;
		this.size = size;
		this.isPerson = false;
	}
	
	/**
	 * Vraci typ hrace (PC, osoba).
	 * @return - Jestli je hrac clovek, vrati true. Jinak false.
	 */
	@Override
	public boolean isPerson() {
		return this.isPerson;
	}
	
	/**
	 * Vrati typ hrace
	 */
	public String getPlayerType() {
		return "PC type 2";
	}
	
	/**
	 * Provede tah za hrace ovladaneho pocitacem. Vyhledavaci algoritmus je jednoduchy. 'Nahodne' se vybere unikatni pole 
	 * z hraci desky. Nasledne se zkontroluje, jestli je toto pole prazdne. Pokud ano, tak se zkontroluje, zda se na toto pole
	 * da vlozit hracuv kamen. Pokud ano, tak se vlozi.
	 * @param board - Hraci deska.
	 * @return - Vraci true v pripade, ze se tah provedl. False v pripade, ze tah nelze provest.
	 */
	public boolean makeMove(Board board) {
		Field field;
		
		ArrayList<Integer> listRow = new ArrayList<Integer>(); // List ocislovanych poli v radku
		ArrayList<Integer> listCol = new ArrayList<Integer>(); // List ocislovanych poli v sloupci
		for (int i = 1; i <= this.size; i++) { // Naplneni
			listRow.add(new Integer(i));
			listCol.add(new Integer(i));
		}
		Collections.shuffle(listRow); // Nahodne zamicha pole X souradnic
		Collections.shuffle(listCol); // Nahodne zamicha pole Y souradnic
		
		for (int y = 0; y < this.size; y++) {
			for (int x = 0; x < this.size; x++) {
				field = board.getField(listRow.get(x), listCol.get(y));
				
				if (field.getDisk() != null) // Je na poli kamen?
					continue;

				for (Direction dirs : Direction.values()) {
					
					if (getNextDisk(dirs, field, this.board.getRules().isFreezing()) > 0) {
						putDisk(field);
						return true; // Kamen byl uspesne vlozen
					}
				}
			}
		}
		return false; // Kamen neni mozno vlozit
	}
	
	/**
     * V urcitem smeru spocita pocet souperovych kamenu.
     * @param dir - Smer, kterym se pokracuje.
     * @param field - Aktualni pole.
     * @param isFreezing - Jestli je aktivovano zamrzani kamenu.
     * @return - Pocet souperovych kamenu nez narazi na kamen hracovy barvy. V opacnem pripade 0.
     */
	@Override
	public int getNextDisk(Direction dir, Field field, boolean isFreezing) {
		int count = 0;
		Field nextField = field;	
		Disk disk;
		
		while (true) {
			nextField = nextField.nextField(dir, this.board);
			
			if (nextField == null) // Zda pole neni okrajove.
				return 0;
			
			disk = nextField.getDisk(); // Disk na tomto poli.
			
			if (disk == null) // Zda neni pole prazdne.
				return 0; // V tomto smeru neni mozne otocit zadne kameny.
			else if (isFreezing && disk.isFreezed())
				return 0;
			else if (this.isWhite != disk.isWhite())
				count++;
			else if (this.isWhite == disk.isWhite())
				return count;
			else
				return 0;
		}
	}

	/**
     * Test barvy hrace.
     * @return - Zda je hrac bily.
     */
	@Override
	public boolean isWhite() {
		return this.isWhite;
	}

	/**
	 * Obrati vsechny kameny v urcitem smeru dokud nenarazi na kamen hracovy barvy.
	 * Predtim musi byt osetreno, ze se v tomto smeru skutecne hracuv kamen nachazi.
	 * @param dir - Smer.
	 * @param field - Aktualni pole.
	 * @return - True v pripade uspechu.
	 */	
	@Override
	public boolean flipAllDisks(Direction dir, Field field) { 
        Field nextField = field.nextField(dir, this.board);
        Disk nextDisk = nextField.getDisk();
        
        if (this.isWhite == nextDisk.isWhite())
            return true;
        else if (this.isWhite != nextDisk.isWhite()) {
        	nextField.getDisk().turn();
            return flipAllDisks(dir, nextField);
        }   
        else
            return false;
	}
	
	/**
     * Vraci jmeno hrace.
     * @return - Vrati jmeno hrace.
     */
    @Override
    public String getName() {
    	return this.name;
    }

	/**
     * Test, zda je mozne vlozit novy kamen hrace na dane pole.
     * Kamen ze sady nevybira ani nevklada na pole.
     * @param field - Pole, na ktere se ma vkladat kamen.
     * @return - Zda je mozne kamen vlozit.
     */
	@Override
	public boolean canPutDisk(Field field) {
        Field nextField = field;
        boolean canPut = false;
        
        if (field.getDisk() == null) {
            for (Direction dir : Direction.values()) {
            	if (getNextDisk(dir, nextField, false) > 0) {
                	canPut = true;
                }
            }
        }
        return canPut;
	}
	
	/**
     * Test, zda je mozne vlozit novy kamen hrace na dane pole v pripade, ze je aktivovano zamrzani kamenu.
     * Kamen ze sady nevybira ani nevklada na pole.
     * @param field - Pole, na ktere se ma vkladat kamen.
     * @return - Zda je mozne kamen vlozit.
     */
    @Override
	public boolean canPutDiskFreeze(Field field) {
        Field nextField = field;
        boolean canPut = false;
        
        if (field.getDisk() == null) {
            for (Direction dir : Direction.values()) {
            	if (getNextDisk(dir, nextField, true) > 0) {
                	canPut = true;
                }
            }
        }
        return canPut;
    }

	/**
     * Test prazdnosti sady kamenu, ktere ma hrac k dispozici.
     * @return - Zda je sada prazdna.
     */
	@Override
	public boolean emptyPool() {
		if (this.pool == null || this.pool.empty())
            return true;
        else
            return false;
	}

	/**
     * Vlozi novy kamen hrace na dane pole, pokud to pravidla umoznuji.
     * Pokud lze vlozit, vybere kamen ze sady a vlozi na pole.
     * @param field - Pole, na ktere se vklada kamen.
     * @return - Uspech akce.
     */
	@Override
	public boolean putDisk(Field field) {
        Disk putDisk;
        
        if (this.board.getRules().isFreezing()) {
        	if (!canPutDiskFreeze(field))
                return false; // Pravidla neumoznuji vlozit kamen.
        }
        else {
        	if (!canPutDisk(field))
                return false; // Pravidla neumoznuji vlozit kamen.
        }
        
        if (!this.emptyPool()) {
            Field nextField = field;
        
            if (field.getDisk() == null) {
                for (Direction dir : Direction.values()) {
                	if (getNextDisk(dir, nextField, false) > 0) {
                        // Obrat kameny v tomto smeru.
                        flipAllDisks(dir, field);
                    }
                }
            }
            putDisk = (Disk)this.pool.pop();        
            return field.putDisk(putDisk);   
        }
        else
            return false;
	}

	/**
     * Inicializace hrace v ramci hraci desky.
     * Vytvori sadu kamenu o prislusne velikosti a umisti sve pocatecni kameny na desku.
     * @param board - Deska, v ramci ktere se hrac inicializuje.
     */
	@Override
	public void init(Board board) {
	       int poolSize = board.getSize() * board.getSize();
	        this.pool = new Stack<Disk>();
	        
	        for (int i = 0; i < poolSize; i++) {
	            this.pool.push(new Disk(this.isWhite));
	        }
	        
	        int center = board.getSize() / 2;
	        Disk disk; // Vkladany disk.
	        Field field; // Policko, na ktere se vklada disk.
	        boolean putDisk; // Pro ulozeni navratove hodnoty pri volani metody putDisk();
	        
	        
	        if (this.isWhite) {
	            field = board.getField(center, center);
	            disk = (Disk)this.pool.pop();
	            putDisk = field.putDisk(disk);
	            if (!putDisk) System.out.println("Nepodarilo se vlozit kamen.");
	            
	            field = board.getField(center+1, center+1);          
	            disk = (Disk)this.pool.pop();
	            putDisk = field.putDisk(disk);
	            if (!putDisk) System.out.println("Nepodarilo se vlozit kamen.");
	        }
	        else {
	            field = board.getField(center+1, center);
	            disk = (Disk)this.pool.pop();
	            putDisk = field.putDisk(disk);
	            if (!putDisk) System.out.println("Nepodarilo se vlozit kamen.");
	            
	            field = board.getField(center, center+1);          
	            disk = (Disk)this.pool.pop();
	            putDisk = field.putDisk(disk);
	            if (!putDisk) System.out.println("Nepodarilo se vlozit kamen.");
	        }
		
	}
	
	/**
	 * Dostupny pocet kamenu hrace
	 * @return Pocet kamenu
	 */
	public int getNumDisks() {
		return pool.size();
	}
	
	/**
	 * Nastavi pocet kamenu hrace
	 */
	public void setNumDisks(int numDisks) {
		//Vyhazej stare kameny
		while (pool.size() > 0)
			pool.pop();
		
		//Vytvor nove
		for (int i = 0; i < numDisks; i++) {
            this.pool.push(new Disk(this.isWhite));
        }
	}
	
	/**
	 * Vrati hraci zpet jeden kamen v pripade undo operace 
	 */
	public void undoDisk() {
		this.pool.push(new Disk(this.isWhite));
	}

}

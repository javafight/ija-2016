package game;

import board.Board;
import board.Field;
import board.Field.Direction;

/**
*
* @author Milos Dolinsky, Pavel Mencner
* Rozhrani reprezentujici hrace.
* 
*/
public interface Player {
	
	/**
     * Pokracuje v urcitem smeru v sekvenci kamenu souperovi barvy dokud nenarazi na okraj desky, prazdne pole nebo kamen hracovy barvy.
     * Sekvenci vyhledava rekurzivne.
     * @param dir - Smer, kterym se pokracuje.
     * @param field - Aktualni pole.
     * @param isFreezing - Jestli je aktivovano zamrzani kamene.
     * @return - Vraci 0, kdyz se narazi na okraj desky nebo prazdne pole. 1, kdyz se narazi na kamen hracovy barvy.
     */
	public int getNextDisk(Direction dir, Field field, boolean isFreezing);
	
	/**
	 * Zda je hrac clovek
	 * @return - true kdyz je hrac clovek.
	 */
	public boolean isPerson();
	
	/**
	 * Vrati typ hrace
	 */
	public String getPlayerType();
	
	/**
	 * Provede tah za hrace ovladaneho pocitacem.
	 * @param board - Hraci deska.
	 * @return - True v pripade, ze je mozno provest tah. False v pripade, ze to mozno neni.
	 */
	public boolean makeMove(Board board);
	
	/**
     * Test barvy hrace.
     * @return - Zda je hrac bily.
     */
	public boolean isWhite();

	/**
    * Obrati vsechny kameny v urcitem smeru dokud nenarazi na kamen hracovy barvy.
    * Predtim musi byt osetreno, ze se v tomto smeru skutecne hracuv kamen nachazi.
    * @param dir - Smer.
    * @param field - Aktualni pole.
    * @return - True v pripade uspechu.
    */
	public boolean flipAllDisks(Direction dir, Field field);

    /**
     * Test, zda je mozne vlozit novy kamen hrace na dane pole.
     * Kamen ze sady nevybira ani nevklada na pole.
     * @param field - Pole, na ktere se ma vkladat kamen.
     * @return - Zda je mozne kamen vlozit.
     */
	public boolean canPutDisk(Field field);
	
	/**
     * Test, zda je mozne vlozit novy kamen hrace na dane pole v pripade, ze je aktivovano zamrzani kamenu.
     * Kamen ze sady nevybira ani nevklada na pole.
     * @param field - Pole, na ktere se ma vkladat kamen.
     * @return - Zda je mozne kamen vlozit.
     */
	public boolean canPutDiskFreeze(Field field);

    /**
     * Test prazdnosti sady kamenu, ktere ma hrac k dispozici.
     * @return - Zda je sada prazdna.
     */
	public boolean emptyPool();

    /**
     * Vlozi novy kamen hrace na dane pole, pokud to pravidla umoznuji.
     * Pokud lze vlozit, vybere kamen ze sady a vlozi na pole.
     * @param field - Pole, na ktere se vklada kamen.
     * @return - Uspech akce.
     */
	public boolean putDisk(Field field);

    /**
     * Inicializace hrace v ramci hraci desky.
     * Vytvori sadu kamenu o prislusne velikosti a umisti sve pocatecni kameny na desku.
     * @param board - Deska, v ramci ktere se hrac inicializuje.
     */
	public void init(Board board);
	
	/**
	 * Vrati jmeno hrace.
	 * @return - Jmeno hrace.
	 */
	public String getName();

	/**
	 * Dostupny pocet kamenu hrace
	 * @return Pocet kamenu
	 */
	public int getNumDisks();
	
	
	/**
	 * Nastavi pocet kamenu hrace
	 * @param numDisks - Pocet disku.
	 */
	public void setNumDisks(int numDisks);
	
	/**
	 * Vrati hraci zpet jeden kamen v pripade undo operace 
	 */
	public void undoDisk();
	
}

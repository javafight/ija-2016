package game;

import java.util.Stack;

import board.Board;
import board.Disk;
import board.Field;
import board.Field.Direction;

/**
*
* @author Milos Dolinsky, Pavel Mencner
* Reprezentuje hrace ovladaneho clovekem.
* 
*/
public class PlayerPerson implements Player {
	private boolean isWhite;
	private Stack<Disk> pool;
	private String name;
	private Board board;
	private boolean isPerson;
	
	/**
     * Inicializace hrace.
     * @param isWhite - Urcuje barvu hrace (true = bily, false = cerny) 
     * @param name - Jmeno
     * @param board - Hraci deska.
     */
    public PlayerPerson(boolean isWhite, String name, Board board) {
        this.isWhite = isWhite;
        this.name = name;
        this.board = board;
        this.isPerson = true;
    }
    
    /**
     * Vraci typ hrace.
     * @return - True v pripade cloveka, false v pripade pocitace.
     */
    @Override
    public boolean isPerson() {
    	return this.isPerson;
    }
    
	/**
	 * Vrati typ hrace
	 */
	public String getPlayerType() {
		return "Person";
	}
    
    /**
     * V pripade cloveka nepotrebna metoda.
     * @param board - hraci deska.
     * @return - Vzdy false.
     */
    @Override
    public boolean makeMove(Board board) {
    	return false;
    }
    
    /**
     * Vraci jmeno hrace.
     * @return - Vrati jmeno hrace.
     */
    @Override
    public String getName() {
    	return this.name;
    }
	
    /**
     * Pokracuje v urcitem smeru v sekvenci kamenu souperovi barvy dokud nenarazi na okraj desky, prazdne pole nebo kamen hracovy barvy.
     * Sekvenci vyhledava rekurzivne.
     * @param dir - Smer, kterym se pokracuje.
     * @param field - Aktualni pole.
     * @param isFreezing - Jestli je povoleno zamrzani.
     * @return - Vraci 0, kdyz se narazi na okraj desky nebo prazdne pole. 1, kdyz se narazi na kamen hracovy barvy.
     */
    @Override
	public int getNextDisk(Direction dir, Field field, boolean isFreezing) {
		Field nextField = field.nextField(dir, this.board); // Dalsi pole v tomto smeru.
        final int y = 1, n = 0; // Pravda, nepravda. 
		
        if (nextField == null) // Test, zda toto pole neni okrajove.
            return n;
        
        Disk nextDisk = nextField.getDisk(); // Disk na tomto poli.
        
        if (nextDisk == null) // Zda neni pole prazdne.
            return n;
        
        else if (this.isWhite != nextDisk.isWhite()) // Zda na poli je souperuv kamen.
            return getNextDisk(dir, nextField, isFreezing);
        else if (this.isWhite == nextDisk.isWhite()) // Zda na poli je hracuv kamen.
            return y;
        else
            return n;
	}
    
    public int getNextDiskFreeze(Direction dir, Field field, boolean isFreezing) {
		Field nextField = field.nextField(dir, this.board); // Dalsi pole v tomto smeru.
        int y = 1, n = 0; // Pravda, nepravda. 
		
        if (nextField == null) // Test, zda toto pole neni okrajove.
            return n;
        
        Disk nextDisk = nextField.getDisk(); // Disk na tomto poli.
        
        
        if (nextDisk != null && nextDisk.isFreezed())
        	return n;
        
        if (nextDisk == null) // Zda neni pole prazdne.
            return n;
        else if (this.isWhite != nextDisk.isWhite()) // Zda na poli je souperuv kamen.
            return getNextDisk(dir, nextField, isFreezing);
        else if (this.isWhite == nextDisk.isWhite()) // Zda na poli je hracuv kamen.
            return y;
        else
            return n;
	}

    /**
     * Test barvy hrace.
     * @return - Zda je hrac bily.
     */
    @Override
	public boolean isWhite() {
		return this.isWhite;
	}

	/**
    * Obrati vsechny kameny v urcitem smeru dokud nenarazi na kamen hracovy barvy.
    * Predtim musi byt osetreno, ze se v tomto smeru skutecne hracuv kamen nachazi.
    * @param dir - Smer.
    * @param field - Aktualni pole.
    * @return - True v pripade uspechu.
    */
    @Override
	public boolean flipAllDisks(Direction dir, Field field) {
        Field nextField = field.nextField(dir, board);
        Disk nextDisk = nextField.getDisk();
        
        if (this.isWhite == nextDisk.isWhite())
            return true;
        else if (this.isWhite != nextDisk.isWhite()){
        	nextField.getDisk().turn();
            return flipAllDisks(dir, nextField);
        }
        else
            return false;
	}

    /**
     * Test, zda je mozne vlozit novy kamen hrace na dane pole.
     * Kamen ze sady nevybira ani nevklada na pole.
     * @param field - Pole, na ktere se ma vkladat kamen.
     * @return - Zda je mozne kamen vlozit.
     */
    @Override
	public boolean canPutDisk(Field field) {
       	Disk nextDisk;
        Field nextField;
        boolean canPut = false;

        if (field.getDisk() == null) {
            for (Direction dir : Direction.values()) {
                /*        
            	if (getNextDisk(dir, field, false) != 0) {
                        	canPut = true;
                        }
                        */
                if ((nextField = field.nextField(dir, this.board)) != null && (nextDisk = nextField.getDisk()) != null) {
                    if (this.isWhite != nextDisk.isWhite()) {
                        if (getNextDisk(dir, nextField, true) != 0) {
                        	canPut = true;
                        }
                    }
                }
            	
            }
        }
        return canPut;
	}
    
    /**
     * Test, zda je mozne vlozit novy kamen hrace na dane pole v pripade, ze je aktivovano zamrzani kamenu.
     * Kamen ze sady nevybira ani nevklada na pole.
     * @param field - Pole, na ktere se ma vkladat kamen.
     * @return - Zda je mozne kamen vlozit.
     */
    @Override
	public boolean canPutDiskFreeze(Field field) {
    	Disk nextDisk;
        Field nextField;
        boolean canPut = false;
        
        if (field.getDisk() == null) {
            for (Direction dir : Direction.values()) {
                if ((nextField = field.nextField(dir, this.board)) != null && (nextDisk = nextField.getDisk()) != null && !nextDisk.isFreezed()) {
                    if (this.isWhite != nextDisk.isWhite()) {
                        if (getNextDiskFreeze(dir, nextField, true) != 0) {
                        	canPut = true;
                        }
                    }
                }
            }
        }
        return canPut;
    }

    /**
     * Test prazdnosti sady kamenu, ktere ma hrac k dispozici.
     * @return - Zda je sada prazdna.
     */
    @Override
	public boolean emptyPool() {
        if (this.pool == null || this.pool.empty())
            return true;
        else
            return false;
	}

    /**
     * Vlozi novy kamen hrace na dane pole, pokud to pravidla umoznuji.
     * Pokud lze vlozit, vybere kamen ze sady a vlozi na pole.
     * @param field - Pole, na ktere se vklada kamen.
     * @return - Uspech akce.
     */
    @Override
	public boolean putDisk(Field field) {
        Disk putDisk;
        
        
        if (this.board.getRules().isFreezing()) {
        	if (!canPutDiskFreeze(field))
                return false; // Pravidla neumoznuji vlozit kamen.
        }
        else {
        	if (!canPutDisk(field))
                return false; // Pravidla neumoznuji vlozit kamen.
        }
        
        
        if (!this.emptyPool()) {
            if (field.getDisk() == null) {
                for (Direction dir : Direction.values()) {
                	if (getNextDiskFreeze(dir, field, false) != 0) {
                		// Obrat kameny v tomto smeru.
                        flipAllDisks(dir, field);
                    }
                }
            }
            putDisk = (Disk)this.pool.pop();        
            return field.putDisk(putDisk);
        }
        else
            return false;
	}
    

    /**
     * Inicializace hrace v ramci hraci desky.
     * Vytvori sadu kamenu o prislusne velikosti a umisti sve pocatecni kameny na desku.
     * @param board - Deska, v ramci ktere se hrac inicializuje.
     */
    @Override
	public void init(Board board) {
	       int poolSize = board.getSize() * board.getSize();
	        this.pool = new Stack<Disk>();
	        
	        for (int i = 0; i < poolSize; i++) {
	            this.pool.push(new Disk(this.isWhite));
	        }
	        
	        int center = board.getSize() / 2;
	        Disk disk; // Vkladany disk.
	        Field field; // Policko, na ktere se vklada disk.
	        boolean putDisk; // Pro ulozeni navratove hodnoty pri volani metody putDisk();
	        
	        
	        if (this.isWhite) {
	            field = board.getField(center, center);
	            disk = (Disk)this.pool.pop();
	            putDisk = field.putDisk(disk);
	            if (!putDisk) System.out.println("Nepodarilo se vlozit kamen.");
	            
	            field = board.getField(center+1, center+1);          
	            disk = (Disk)this.pool.pop();
	            putDisk = field.putDisk(disk);
	            if (!putDisk) System.out.println("Nepodarilo se vlozit kamen.");
	        }
	        else {
	            field = board.getField(center+1, center);
	            disk = (Disk)this.pool.pop();
	            putDisk = field.putDisk(disk);
	            if (!putDisk) System.out.println("Nepodarilo se vlozit kamen.");
	            
	            field = board.getField(center, center+1);          
	            disk = (Disk)this.pool.pop();
	            putDisk = field.putDisk(disk);
	            if (!putDisk) System.out.println("Nepodarilo se vlozit kamen.");
	        }
		
	}
	
    @Override
    public String toString() {
        if (this.isWhite())
            return "white";
        else
            return "black";
    }
    
	/**
	 * Dostupny pocet kamenu hrace
	 * @return Pocet kamenu
	 */
	public int getNumDisks() {
		return pool.size();
	}
	
	/**
	 * Nastavi pocet kamenu hrace
	 */
	public void setNumDisks(int numDisks) {
		//Vyhazej stare kameny
		while (pool.size() > 0)
			pool.pop();
		
		//Vytvor nove
		for (int i = 0; i < numDisks; i++) {
            this.pool.push(new Disk(this.isWhite));
        }
	}
	
	/**
	 * Vrati hraci zpet jeden kamen v pripade undo operace 
	 */
	public void undoDisk() {
		this.pool.push(new Disk(this.isWhite));
	}

}

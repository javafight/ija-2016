package app;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

import game.GameModel;

/**
 * 
 * @author Pavel Mencner, Milos Dolinsky
 * Spojuje graficke rozhrani a backend aplikace.
 *
 */
public class AppController {
	private GameModel gameModel;
	private MenuView menuView;
	private GameView gameView;

	AppController(GameModel m, MenuView mv, GameView gv) {
		gameModel = m;
		menuView = mv;
		gameView = gv;

		menuView.addNewGameListener(new NewGameListener());
		menuView.addLoadListener(new LoadListener());
		menuView.addWindowCloseListener(new WindowCloseListener());
		
		gameView.addRestartListener(new RestartListener());
		gameView.addLoadListener(new LoadListener());
		gameView.addSaveListener(new SaveListener());
		gameView.addUndoListener(new UndoListener());
		gameView.addNewWindowListener(new NewWindowListener());
		gameView.addWindowCloseListener(new WindowCloseListener());
		gameView.addPassListener(new PassListener());

		gameModel.addObserver(gameView); // model notify view about change
	}
	
	/**
	 * Po nacteni informaci o nove hre nastavi spravnou velikost hraci desky v GameView
	 */
	private void initBoardView() {
		gameView.resizeBoard();
		
		int boardSize = gameModel.getBoard().getSize();
		
		for (int row = 0; row < boardSize; row++) {
			for (int col = 0; col < boardSize; col++) {
				gameView.addFieldListener(row, col, new FieldListener(row, col));
			}
		}
		
		gameModel.notifyChange();
	}

	/**
	 * Akce zahajeni nove hry
	 */
	class NewGameListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			if (!menuView.getP1Type().equals("Person") && !menuView.getP2Type().equals("Person")) {
				JOptionPane.showMessageDialog(menuView, "One of players must be a person", "Player", JOptionPane.WARNING_MESSAGE);
			} else {
				menuView.setVisible(false);
			
				if (menuView.getFreezeEnabled()) {
					gameModel.startNewGame(menuView.getBoardSize(), menuView.getP1Type(), menuView.getP2Type(), menuView.getP1Name(), menuView.getP2Name(),
							menuView.getFreezeGap(), menuView.getFreezeMaxTime(), menuView.getFreezeNumDisks());
				} else {
					gameModel.startNewGame(menuView.getBoardSize(), menuView.getP1Type(), menuView.getP2Type(), menuView.getP1Name(), menuView.getP2Name());
				}
			
				initBoardView();
				
				gameView.setVisible(true);
				
				gameModel.makeTurnPC();
			}
		}
	}

	/**
	 * Akce otevreni dialogu pro nacteni ulozene hry a zpracovani souboru
	 */
	class LoadListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			JFileChooser openFile = new JFileChooser();
			int retVal = openFile.showOpenDialog(null);

			if (retVal == JFileChooser.APPROVE_OPTION) {
				File file = openFile.getSelectedFile();

				try {
					Scanner s = new Scanner(file);
					s.useDelimiter("\\Z");
					
					String content = s.next();
					s.close();
					
					if (gameModel.setSerializedState(content)) {
					
						menuView.setVisible(false);
						initBoardView();
						gameModel.notifyChange();
						gameView.setVisible(true);
					} else {
						Component parent;
						
						if (gameView.isVisible())
							parent = gameView;
						else
							parent = menuView;
						
						JOptionPane.showMessageDialog(parent, "Can't laod game, save corrupted", "Save corruted", JOptionPane.ERROR_MESSAGE);
					}
				} catch (Exception e1) {
					Component parent;
					
					if (gameView.isVisible())
						parent = gameView;
					else
						parent = menuView;
					
					JOptionPane.showMessageDialog(parent, "Can't laod game, save corrupted", "Save corruted", JOptionPane.ERROR_MESSAGE);
				}
			} else {
				// Zruseny vyber, nedelej nic
			}
		}
	}

	/**
	 * Akce ulozeni soucasne rozehrane hry
	 */
	class SaveListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			String filename = "reversi.save";
			JFileChooser savefile = new JFileChooser();
			savefile.setSelectedFile(new File(filename));

			int sf = savefile.showSaveDialog(savefile);
			BufferedWriter writer;
			if (sf == JFileChooser.APPROVE_OPTION) {
				try {
					writer = new BufferedWriter(new FileWriter(savefile.getSelectedFile()));
					
					writer.write(gameModel.getSerializedState());
					
					writer.close();
					
					JOptionPane.showMessageDialog(gameView, "Game has been saved", "Game Saved", JOptionPane.INFORMATION_MESSAGE);
				} catch (IOException ioe) {
					ioe.printStackTrace();
				}
			} else if (sf == JFileChooser.CANCEL_OPTION) {
				// Zruseny vyber, nedelej nic
			}
		}
	}

	/**
	 * Akce navraceni predchoziho tahu
	 */
	class UndoListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			gameModel.undo();
		}
	}

	/**
	 * Akce restartovani aktualni hry
	 */
	class RestartListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			/*
			if (!menuView.getP1Type().equals("Person") && !menuView.getP2Type().equals("Person")) {
				JOptionPane.showMessageDialog(menuView, "One of players must be a person", "Player", JOptionPane.WARNING_MESSAGE);
			} else {
				if (menuView.getFreezeEnabled()) {
					gameModel.startNewGame(menuView.getBoardSize(), menuView.getP1Type(), menuView.getP2Type(), menuView.getP1Name(), menuView.getP2Name(),
							menuView.getFreezeGap(), menuView.getFreezeMaxTime(), menuView.getFreezeNumDisks());
				} else {
					gameModel.startNewGame(menuView.getBoardSize(), menuView.getP1Type(), menuView.getP2Type(), menuView.getP1Name(), menuView.getP2Name());
				}
				
				gameModel.makeTurnPC();
			}
			*/
			gameModel.restart();
			/*
			gameModel.startNewGame(Integer.toString(gameModel.getBoard().getSize()), playerWhite, playerBlack, p1name, p2name);
			gameModel.makeTurnPC();
			*/
		}
	}

	/**
	 * Akce otevreni noveho okna se samostatnou hrou
	 */
	class NewWindowListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			ReversiApp.newInstance();
		}
	}
	
	/**
	 * Rizeni udalosti zavreni okna s podporou vice soucasne bezicich her, ukoncuje bezici vlakna a posleze i program
	 */
	class WindowCloseListener extends WindowAdapter {
		@Override
		public void windowClosing(WindowEvent e) {
			
			ReversiApp.decrementThreadCOunt(); //sniz pocitadlo referenci na vlakna
			
			if(ReversiApp.getThreadCount() == 0) {
				//Posledni vlakno, zabij cely program
				System.exit(0);
			} else {
				//zustavaji jine vlakna, zabij pouze aktualni vlakno
				Thread.currentThread().interrupt();
			}
		}
	}
	
	/**
	 * Provedeni tahu hracem
	 */
	class FieldListener implements ActionListener {
		private int row;
		private int col;
		
		public FieldListener(int row, int col) {
			this.row = row;
			this.col = col;
		}
		
		public void actionPerformed(ActionEvent e) {
			
			gameModel.makeTurn(row+1, col+1);
		}
	}
	
	/**
	 * Vzdani se tahu pokud neni k dispozici zadny platny
	 */
	class PassListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			gameModel.passTurn();
		}
	}
	
}

package app;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.util.Observable;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import board.Disk;
import game.GameModel;

/**
 * 
 * @author Pavel Mencner, Milos Dolinsky
 * Zobrazeni nove hry.
 *
 */
public class GameView extends JFrame implements java.util.Observer {

	private static final long serialVersionUID = 2891190433015423654L;
	private static final int frameWidth = 550;
	private static final int frameHeight = 550;
	
	private GameModel gameModel;
	
	private Integer updateCounter;
	
	private boolean gameEndShowed;
	
	private int boardSize;
	private int fieldSize;
	
	//Menu
	private JMenuBar menuBar;
	private JMenu gameMenu;
    private JMenuItem saveMenuItem;
	private JMenuItem loadMenuItem;
	private JMenuItem undoMenuItem;
	private JMenuItem restartMenuItem;
	private JMenuItem newWindowMenuItem;
	
	//Actual player info
	private JLabel playerImageLabel;
	private JLabel playerNameLabel;
	private JLabel playerDiskLabel;
	private JLabel playerNumDisksLabel;
	
	//Undo
	private JButton undoButton;
	
	//Display actual score
	private JLabel scoreLabel;
	private JLabel blackDiskLabel;
	private JLabel blackScoreLabel;
	private JLabel whiteDiskLabel;
	private JLabel whiteScoreLabel;
	
	private JButton passButton;
	
	//Layouts etc.
	private JPanel content;
	private GridBagConstraints c;
	private JButton[][] boardButtons;
	
	//Icons
	private ImageIcon fieldIcon;
	private ImageIcon fieldDiskBlackIcon;
	private ImageIcon fieldDiskWhiteIcon;
	private ImageIcon fieldDiskWhiteFrozenIcon;
	private ImageIcon fieldDiskBlackFrozenIcon;
	
	private ImageIcon pcPlayerIcon;
	private ImageIcon personPlayerIcon;
	
	private ImageIcon diskWhiteIcon;
	private ImageIcon diskBlackIcon;
	
	/**
	 * Inicializace pohledu na hru
	 * @param m Herni model
	 */
	GameView(GameModel m) {
		gameModel = m;
		updateCounter = 0;
		boardSize = 0;
		fieldSize = 0;
		this.setResizable(false);
		this.setSize(frameWidth, frameHeight);
		this.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);

		// Create menu
		menuBar = new JMenuBar();

		gameMenu = new JMenu("Game");
		undoMenuItem = new JMenuItem("Undo");
		saveMenuItem = new JMenuItem("Save");
		restartMenuItem = new JMenuItem("Restart");
		loadMenuItem = new JMenuItem("Load");
		newWindowMenuItem = new JMenuItem("New window");
		gameMenu.add(undoMenuItem);
		gameMenu.add(saveMenuItem);
		gameMenu.add(restartMenuItem);
		gameMenu.add(loadMenuItem);
		gameMenu.add(newWindowMenuItem);

		menuBar.add(gameMenu);

		this.setJMenuBar(menuBar);

		// Content
		Font fontBig = new Font("Serif", Font.PLAIN, 32);
		Font fontRegular = new Font("Serif", Font.PLAIN, 28);
		Font fontSmall = new Font("Serif", Font.PLAIN, 18);

		playerImageLabel = new JLabel();
		playerNameLabel = new JLabel();
		playerNameLabel.setFont(fontBig);
		playerDiskLabel = new JLabel();
		playerNumDisksLabel = new JLabel();
		playerNumDisksLabel.setFont(fontSmall);

		undoButton = new JButton("Undo");

		scoreLabel = new JLabel("Score");
		scoreLabel.setFont(fontBig);
		blackDiskLabel = new JLabel();
		blackScoreLabel = new JLabel();
		blackScoreLabel.setFont(fontRegular);
		whiteDiskLabel = new JLabel();
		whiteScoreLabel = new JLabel();
		whiteScoreLabel.setFont(fontRegular);
		
		passButton = new JButton("Pass");
		
		content = new JPanel();
		content.setLayout(new GridBagLayout());

		c = new GridBagConstraints();
		// c.fill = GridBagConstraints.HORIZONTAL;

		c.insets = new Insets(5, 5, 5, 5);
		c.weightx = 1.0;
		c.weighty = 1.0;

		c.gridx = 0;
		c.gridy = 0;
		c.gridwidth = 2;
		c.anchor = GridBagConstraints.NORTHWEST;
		content.add(new PlayerInfoPane(), c);

		c.gridx = 1;
		c.gridy = 1;
		c.gridwidth = 1;
		c.anchor = GridBagConstraints.CENTER;
		content.add(new ScorePane(), c);

		this.setContentPane(content);
		this.setLocationRelativeTo(null); // Frame appear at the center of screen
	}
	
	/**
	 * Zmena velikosti hraci desky napr. nacteni jine hry, volano controllerem 
	 */
	public void resizeBoard() {
		boardSize = gameModel.getBoard().getSize();
		fieldSize = (int)(frameHeight * 0.74 / boardSize);
		
		fieldIcon = getIcon("/resources/field.png", fieldSize, fieldSize);
		fieldDiskBlackIcon = getIcon("/resources/fieldDiskBlack.png", fieldSize, fieldSize);
		fieldDiskWhiteIcon = getIcon("/resources/fieldDiskWhite.png", fieldSize, fieldSize);
		fieldDiskWhiteFrozenIcon = getIcon("/resources/fieldDiskWhiteFrozen.png", fieldSize, fieldSize);
		fieldDiskBlackFrozenIcon = getIcon("/resources/fieldDiskBlackFrozen.png", fieldSize, fieldSize);
		
        c.gridx = 0;
        c.gridy = 1;
        c.gridwidth = 1;
        c.anchor = GridBagConstraints.SOUTHWEST;
        content.add(new BoardPane(), c);
	}
	
	public class PlayerInfoPane extends JPanel {
		
		private static final long serialVersionUID = 1384212415400768016L;
		private static final int avatarSize = 70;
		private static final int diskSize = 40;

		public PlayerInfoPane() {
			
			FlowLayout l = new FlowLayout();
			l.setVgap(0);
			l.setHgap(10);
				
			setLayout(l);
			
			playerImageLabel.setPreferredSize(new Dimension(avatarSize, avatarSize));
			personPlayerIcon = getIcon("/resources/personPlayer.png", avatarSize, avatarSize);
			pcPlayerIcon = getIcon("/resources/pcPlayer.png", avatarSize, avatarSize);
			
			add(playerImageLabel);
			
			playerNameLabel.setPreferredSize(new Dimension(130, avatarSize));
			playerNameLabel.setMaximumSize(new Dimension(130, avatarSize));
			add(playerNameLabel);
			
			playerDiskLabel.setPreferredSize(new Dimension(diskSize, diskSize));
			diskWhiteIcon = getIcon("/resources/diskWhite.png", diskSize, diskSize);
			diskBlackIcon = getIcon("/resources/diskBlack.png", diskSize, diskSize);
			
			add(playerDiskLabel);
			
			playerNumDisksLabel.setPreferredSize(new Dimension(30, diskSize));
			playerNumDisksLabel.setVerticalAlignment(JLabel.BOTTOM);
			add(playerNumDisksLabel);
			
			undoButton.setPreferredSize(new Dimension(70, 40));
			add(undoButton);
		}
	}
	
	public class BoardPane extends JPanel {

		private static final long serialVersionUID = -5572394777310617250L;

		public BoardPane() {
			setLayout(new GridBagLayout());
			
			GridBagConstraints c = new GridBagConstraints();
	        c.fill = GridBagConstraints.HORIZONTAL;
	        c.weightx = 1.0;
	        c.weighty = 1.0;
	        
			boardButtons = new JButton[boardSize][boardSize];
			
			for (int row = 0; row < boardSize; row++) {
				for (int col = 0; col < boardSize; col++) {
					c.gridy = row;
					c.gridx = col;
					boardButtons[row][col] = new JButton();
					
					boardButtons[row][col].setPreferredSize(new Dimension(fieldSize,fieldSize));
					boardButtons[row][col].setMinimumSize(new Dimension(fieldSize,fieldSize));
					
					boardButtons[row][col].setBorderPainted(false);
					boardButtons[row][col].setBorder(null);
					boardButtons[row][col].setMargin(new Insets(0, 0, 0, 0));
					boardButtons[row][col].setContentAreaFilled(false);
					
					add(boardButtons[row][col], c);
				}
			}
		}
	}
	
	public class ScorePane extends JPanel {

		private static final long serialVersionUID = -8943622103663109169L;
		private static final int lblSize = 40;
		
		public ScorePane() {
			setLayout(new GridBagLayout());

			GridBagConstraints c = new GridBagConstraints();
			c.fill = GridBagConstraints.HORIZONTAL;
			c.insets = new Insets(5, 5, 5, 5);
			c.weightx = 1.0;
			c.weighty = 1.0;

			c.gridwidth = 2;
			c.gridx = 0;
			c.gridy = 0;
			add(scoreLabel, c);

			c.gridwidth = 1;
			c.gridx = 0;
			c.gridy = 1;
			whiteDiskLabel.setPreferredSize(new Dimension(lblSize, lblSize));
			whiteDiskLabel.setIcon(getIcon("/resources/diskWhite.png", lblSize, lblSize));
			add(whiteDiskLabel, c);
			
			c.gridx = 1;
			c.gridy = 1;
			whiteScoreLabel.setPreferredSize(new Dimension(50, lblSize));
			whiteScoreLabel.setMinimumSize(new Dimension(50, lblSize));
			add(whiteScoreLabel, c);
			
			c.gridx = 0;
			c.gridy = 2;
			blackDiskLabel.setPreferredSize(new Dimension(lblSize, lblSize));
			blackDiskLabel.setIcon(getIcon("/resources/diskBlack.png", lblSize, lblSize));
			add(blackDiskLabel, c);
			
			c.gridx = 1;
			c.gridy = 2;
			add(blackScoreLabel, c);
			
			c.gridwidth = 2;
			c.gridx = 0;
			c.gridy = 3;
			passButton.setPreferredSize(new Dimension(70,40));
			add(passButton, c);
		}
	}
	
	private ImageIcon getIcon(String path, int sizeX, int sizeY) {
		ImageIcon ii = new ImageIcon(getClass().getResource(path));
		
		Image img = ii.getImage() ;  
		Image newimg = img.getScaledInstance(sizeX, sizeY, java.awt.Image.SCALE_SMOOTH);  
		ii = new ImageIcon(newimg);
		
		return ii;
	}
	
	/**
	 * Aktulizace pohledu zalozena na zmene pozorovaneho herniho modelu
	 */
	public void update(Observable obs, Object obj) {
		updateCounter++;
		
		boolean gameEnd = gameModel.gameEnd();
		
		if (!gameEnd)
			gameEndShowed = false;
		
		playerNameLabel.setText(gameModel.currentPlayer().getName());
		
		if (gameModel.currentPlayer().isPerson())
			playerImageLabel.setIcon(personPlayerIcon);
		else
			playerImageLabel.setIcon(pcPlayerIcon);
		
		if (gameModel.currentPlayer().isWhite())
			playerDiskLabel.setIcon(diskWhiteIcon);
		else
			playerDiskLabel.setIcon(diskBlackIcon);
		
		playerNumDisksLabel.setText(Integer.toString(gameModel.currentPlayer().getNumDisks()));
		
		if (gameModel.canUndo() && !gameEnd && gameModel.currentPlayer().isPerson()) {
			undoButton.setEnabled(true);
			undoMenuItem.setEnabled(true);
		} else {
			undoButton.setEnabled(false);
			undoMenuItem.setEnabled(false);
		}
		
		whiteScoreLabel.setText(Integer.toString(gameModel.getScore(true)));
		blackScoreLabel.setText(Integer.toString(gameModel.getScore(false)));
		
		if (gameModel.currentPlayer().isPerson())
			passButton.setEnabled(true);
		else
			passButton.setEnabled(false);
		
		if (gameModel.isPossibleTurn()) {
			passButton.setVisible(false);
		}else {
			if (gameModel.gameEnd())
				passButton.setVisible(false);
			else
				passButton.setVisible(true);
		}
		
		for (int row = 0; row < boardSize; row++) {
			for (int col = 0; col < boardSize; col++) {

				Disk disk = gameModel.getBoard().getField(row+1, col+1).getDisk();
				
				//Zadny disk na hracim poli
				if (disk == null) {
					boardButtons[row][col].setIcon(fieldIcon);
					boardButtons[row][col].setDisabledIcon(fieldIcon);
					
					if (gameModel.currentPlayer().isPerson() && gameModel.currentPlayer().canPutDiskFreeze(gameModel.getBoard().getField(row+1, col+1)))
						boardButtons[row][col].setEnabled(true); //Pokud jde udelat tah a nehraje PC tak enabled
					else
						boardButtons[row][col].setEnabled(false);
					
					if (gameModel.currentPlayer().isWhite()) {
						boardButtons[row][col].setRolloverIcon(fieldDiskWhiteIcon);
						boardButtons[row][col].setPressedIcon(fieldDiskWhiteIcon);
					} else {
						boardButtons[row][col].setRolloverIcon(fieldDiskBlackIcon);
						boardButtons[row][col].setPressedIcon(fieldDiskBlackIcon);
					}
					
					continue;
				}
				
				if (disk.isFreezed()) {
					if (disk.isWhite()) {
						boardButtons[row][col].setIcon(fieldDiskWhiteFrozenIcon);
						boardButtons[row][col].setDisabledIcon(fieldDiskWhiteFrozenIcon);
					} else {
						boardButtons[row][col].setIcon(fieldDiskBlackFrozenIcon);
						boardButtons[row][col].setDisabledIcon(fieldDiskBlackFrozenIcon);
					}
					boardButtons[row][col].setEnabled(false);
					continue;
				}
				
				if (disk.isWhite()) {
					boardButtons[row][col].setIcon(fieldDiskWhiteIcon);
					boardButtons[row][col].setDisabledIcon(fieldDiskWhiteIcon);
					boardButtons[row][col].setEnabled(false);
					continue;
				}
				
				if (!disk.isWhite()) {
					boardButtons[row][col].setIcon(fieldDiskBlackIcon);
					boardButtons[row][col].setDisabledIcon(fieldDiskBlackIcon);
					boardButtons[row][col].setEnabled(false);
					continue;
				}
				
			}
		}
		
		if (gameEnd && !gameEndShowed) {
			gameEndShowed = true;
			String message = "";
			String title = "";
			
			int whiteScore = gameModel.getScore(true);
			int blackScore = gameModel.getScore(false); 
			
			if (whiteScore > blackScore) {
				title = "White player won";
				message = gameModel.getPlayer(true).getName() + " (white) won";
			} else if (whiteScore < blackScore) {
				title = "Black player won";
				message = gameModel.getPlayer(false).getName() + " (black) won";
			} else {
				title = "Draw";
				message = "Draw";
			}
			
			message += "\n\nFinal score:\n\nWhite: " + gameModel.getScore(true) + "\nBlack: " + gameModel.getScore(false);
			
			
			JOptionPane.showMessageDialog(this, message, title, JOptionPane.INFORMATION_MESSAGE);
		}
		
		System.out.println("Update: " + updateCounter);
	}

	public void addRestartListener(ActionListener al) {
		restartMenuItem.addActionListener(al);
	}
	
	public void addUndoListener(ActionListener al) {
		undoMenuItem.addActionListener(al);
		undoButton.addActionListener(al);
	}
	
	public void addSaveListener(ActionListener al) {
		saveMenuItem.addActionListener(al);
	}
	
	public void addLoadListener(ActionListener al) {
		loadMenuItem.addActionListener(al);
	}
	
	public void addNewWindowListener(ActionListener al) {
		newWindowMenuItem.addActionListener(al);
	}
	
	public void addWindowCloseListener(WindowAdapter wcl) {
		this.addWindowListener(wcl);
	}
	
	public void addFieldListener(int row, int col, ActionListener al) {
		boardButtons[row][col].addActionListener(al);
	}
	
	public void addPassListener(ActionListener al) {
		passButton.addActionListener(al);
	}
}

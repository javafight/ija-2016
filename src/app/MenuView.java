package app;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.util.Observable;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import game.GameModel;

/**
 * 
 * @author Pavel Mencner, Milos Dolinsky
 * Zobrazeni menu.
 *
 */
public class MenuView extends JFrame implements java.util.Observer{

	private static final long serialVersionUID = 4224966538307227109L;

	private GameModel gameModel;
	
	private final static int colWidth = 150; 
	//Menu active components
	private JComboBox<String> p1Type;
	private JTextField p1Name;
	private JComboBox<String> p2Type;
	private JTextField p2Name;
	private JComboBox<String> boardSize;
	private JCheckBox freeze;
	private JComboBox<String> freezeGap;
	private JComboBox<String> freezeMaxTime;
	private JComboBox<String> freezeNumDisks;
	
	private JLabel diskFreezeLabel;
	private JLabel freezeNumDisksLabel;
	private JLabel freezeMaxTimeLabel;
	private JLabel freezeGapLabel;
	
	private JButton buttonNewGame;
	private JButton buttonLoad;
	
	/**
	 * Vytvori formular pro nastaveni vlastnosti nove hry
	 * @param m Herni model
	 */
	MenuView(GameModel m) {
		gameModel = m;
		
        //Common stuff
        Font fontBig = new Font("Serif", Font.PLAIN, 22);
        Font fontRegular = new Font("Serif", Font.PLAIN, 14);
		int actY = 0;
        
		this.setResizable(false);
		this.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		
        JPanel content = new JPanel();
		content.setBorder(new EmptyBorder(10, 10, 10, 10)); //frame margins
        content.setLayout(new GridBagLayout());
        
        GridBagConstraints c = new GridBagConstraints();
        c.fill = GridBagConstraints.HORIZONTAL;
        c.insets = new Insets(5,5,5,5);
        c.weightx = 1.0;
        c.weighty = 1.0;
        
        //////////////////////////////////////////////////////////////////
        //Player white
        JLabel playerWhite = new JLabel("Player (white)");
        playerWhite.setFont(fontBig);
        
        c.gridwidth = 2;
        c.gridx = 0;
        c.gridy = actY;
        content.add(playerWhite, c);
        
        //Player type
        p1Type = new JComboBox<>(gameModel.getPlayerTypes());
        p1Type.setFont(fontRegular);
        c.gridwidth = 2;
        c.gridx = 0;
        c.gridy = ++actY;
        content.add(p1Type, c);
        
        //Player Name
        JLabel labelName1 = new JLabel("Name");
        labelName1.setFont(fontRegular);
        
        c.gridwidth = 1;
        c.gridx = 0;
        c.gridy = ++actY;
        content.add(labelName1, c);
        
        p1Name = new JTextField("Player1");
        p1Name.setFont(fontRegular);
        p1Name.setPreferredSize(new Dimension(colWidth, 30));
        
        c.gridx = 1;
        c.gridy = actY;
        content.add(p1Name, c);
        
        
        //Player black
        JLabel playerBlack = new JLabel("Player (black)");
        playerBlack.setFont(fontBig);
        
        c.gridwidth = 2;
        c.gridx = 0;
        c.gridy = ++actY;
        content.add(playerBlack, c);
        
        //Player type
        p2Type = new JComboBox<>(gameModel.getPlayerTypes());
        p2Type.setFont(fontRegular);
        
        c.gridwidth = 2;
        c.gridx = 0;
        c.gridy = ++actY;
        content.add(p2Type, c);
        
        //Player Name
        JLabel labelName2 = new JLabel("Name");
        labelName2.setFont(fontRegular);
        
        c.gridwidth = 1;
        c.gridx = 0;
        c.gridy = ++actY;
        content.add(labelName2, c);
        
        p2Name = new JTextField("Player2");
        p2Name.setFont(fontRegular);
        p2Name.setPreferredSize(new Dimension(colWidth, 30));
        
        c.gridx = 1;
        c.gridy = actY;
        content.add(p2Name, c);
        
        
        //Game settings
        JLabel gameSetting = new JLabel("Game");
        gameSetting.setFont(fontBig);
        
        c.gridwidth = 2;
        c.gridx = 0;
        c.gridy = ++actY;
        content.add(gameSetting, c);
        
        //Board Size
        JLabel boardSizeLabel = new JLabel("Board size");
        boardSizeLabel.setFont(fontRegular);
        
        c.gridwidth = 1;
        c.gridx = 0;
        c.gridy = ++actY;
        content.add(boardSizeLabel, c);
        
        boardSize = new JComboBox<>(gameModel.getBoardSizes());
        boardSize.setFont(fontRegular);
        boardSize.setSelectedIndex(1);
        
        c.gridx = 1;
        c.gridy = actY;
        content.add(boardSize, c);
        
        //Disk freezing
        diskFreezeLabel = new JLabel("Disk freezing");
        diskFreezeLabel.setFont(fontRegular);
        
        c.gridx = 0;
        c.gridy = ++actY;
        content.add(diskFreezeLabel, c);
        
        freeze = new JCheckBox();
        freeze.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                freezeChange();
            }
        });
        
        c.gridx = 1;
        c.gridy = actY;
        content.add(freeze, c);
        
        freezeGapLabel = new JLabel("Freezing gaps (s)");
        freezeGapLabel.setFont(fontRegular);
        freezeGapLabel.setEnabled(false);
        
        c.gridx = 0;
        c.gridy = ++actY;
        content.add(freezeGapLabel, c);
        
        freezeGap = new JComboBox<>(gameModel.getFreezeGaps());
        freezeGap.setFont(fontRegular);
        freezeGap.setSelectedIndex(5);
        freezeGap.setEnabled(false);
        
        c.gridx = 1;
        c.gridy = actY;
        content.add(freezeGap, c);
        
        freezeMaxTimeLabel = new JLabel("Freeze max time (s)");
        freezeMaxTimeLabel.setFont(fontRegular);
        freezeMaxTimeLabel.setEnabled(false);
        
        c.gridx = 0;
        c.gridy = ++actY;
        content.add(freezeMaxTimeLabel, c);
        
        freezeMaxTime = new JComboBox<>(gameModel.getFreezeMaxTime());
        freezeMaxTime.setFont(fontRegular);
        freezeMaxTime.setSelectedIndex(5);
        freezeMaxTime.setEnabled(false);
        
        c.gridx = 1;
        c.gridy = actY;
        content.add(freezeMaxTime, c);
        
        freezeNumDisksLabel = new JLabel("Freeze number disks");
        freezeNumDisksLabel.setFont(fontRegular);
        freezeNumDisksLabel.setEnabled(false);
        
        c.gridx = 0;
        c.gridy = ++actY;
        content.add(freezeNumDisksLabel, c);
        
        freezeNumDisks = new JComboBox<>(gameModel.getFreezeNumDisks());
        freezeNumDisks.setFont(fontRegular);
        freezeNumDisks.setSelectedIndex(1);
        freezeNumDisks.setEnabled(false);
        
        c.gridx = 1;
        c.gridy = actY;
        content.add(freezeNumDisks, c);
        
        //Buttons
        buttonNewGame = new JButton("Start game");
        buttonNewGame.setFont(fontRegular);
        buttonNewGame.setPreferredSize(new Dimension(colWidth, 30));
        
        c.gridx = 0;
        c.gridy = ++actY;
        content.add(buttonNewGame, c);
        
        buttonLoad = new JButton("Load");
        buttonLoad.setFont(fontRegular);
        buttonLoad.setPreferredSize(new Dimension(colWidth, 30));
        
        c.gridx = 1;
        c.gridy = actY;
        content.add(buttonLoad, c);
        
        this.setContentPane(content);
        this.pack();
        
		this.setLocationRelativeTo(null); //Frame appear at the center of screen
		
	}
	
	/**
	 * Zmena volby zamrzani kamenu, aktivuje/deaktivuje volby na teto zavisle
	 */
	private void freezeChange() {
		freezeGapLabel.setEnabled(freeze.isSelected());
		freezeGap.setEnabled(freeze.isSelected());
		freezeMaxTimeLabel.setEnabled(freeze.isSelected());
		freezeMaxTime.setEnabled(freeze.isSelected());
		freezeNumDisksLabel.setEnabled(freeze.isSelected());
		freezeNumDisks.setEnabled(freeze.isSelected());
	}
	
	/**
	 * Aktulizace pohledu zalozena na zmene pozorovaneho modelu
	 */
	public void update(Observable obs, Object obj) {
		
	}
	
	public String getP1Name() {
		return p1Name.getText();
	}
	
	public String getP2Name() {
			return p2Name.getText();
	}

	public String getP1Type() {
		return p1Type.getSelectedItem().toString();
	}
	
	public String getP2Type() {
		return p2Type.getSelectedItem().toString();
	}
	
	public String getBoardSize() {
		return boardSize.getSelectedItem().toString();
	}
	
	public boolean getFreezeEnabled() {
		return freeze.isSelected();
	}
	
	public String getFreezeGap() {
		return freezeGap.getSelectedItem().toString();
	}
	
	public String getFreezeMaxTime() {
		return freezeMaxTime.getSelectedItem().toString();
	}
	
	public String getFreezeNumDisks() {
		return freezeNumDisks.getSelectedItem().toString();
	}
	
	/**
	 * Prida ActionListener pro tlacitko nacteni hry
	 * @param al ActionListener
	 */
	public void addLoadListener(ActionListener al) {
		buttonLoad.addActionListener(al);
	}
	
	/**
	 * Prida ActionListener pro tlacitko strat nove hry
	 * @param al ActionListener
	 */
	public void addNewGameListener(ActionListener al) {
		buttonNewGame.addActionListener(al);
	}
	
	public void addWindowCloseListener(WindowAdapter wcl) {
		this.addWindowListener(wcl);
	}
	
}

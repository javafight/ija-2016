package app;

import board.Board;
import game.GameModel;
import game.ReversiRules;

/**
 * 
 * @author Pavel Mencner, Milos Dolinsky
 * Pro vlakna spustenych her.
 *
 */
public class AppThread extends Thread {
	
	@Override
	public void run() {
		GameModel gameModel = new GameModel();
		
		MenuView menuView = new MenuView(gameModel);
		GameView gameView = new GameView(gameModel);
		
		@SuppressWarnings("unused")
		AppController appController = new AppController(gameModel, menuView, gameView);
		
		menuView.setVisible(true);
	}
}
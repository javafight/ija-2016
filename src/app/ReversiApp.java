package app;

/**
 * 
 * @author Pavel Mencner, Milos Dolinsky
 * Hlavni trida aplikace. Provadi spusteni aplikace.
 *
 */
public class ReversiApp {
	private static int threadCount = 0; //Count active threads
	
	public static void newInstance() {
		threadCount++;
		AppThread t = new AppThread();
		t.start();
	}
	
	public static int getThreadCount() {
		return threadCount;
	}
	
	public static void decrementThreadCOunt() {
		threadCount--;
	}

	public static void main(String[] args) {
		newInstance();
	}

}

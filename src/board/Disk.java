/*
 * Autori: Milos Dolinsky, xdolin01
 *         Pavel Mencner, xmencn00
 * IJA 2015/2016
 * Trida Disk reprezentujici hraci kameny.
 */
package board;

import java.util.Objects;

/**
 * 
 * @author Pavel Mencner, Milos Dolinsky
 * Herni kamen a operace nad nim.
 *
 */
public class Disk {
    private boolean white;
    private boolean isFreezed;
    
    /**
     * Inicializace kamene.
     * @param isWhite - Nastaveni barvy - true je bila, false je cerna.
     */
    public Disk(boolean isWhite) {
        this.white = isWhite;
        this.isFreezed = false;
    }
    
    /**
     * Inicializace kamene pro operaci undo.
     * @param disk - kamen.
     */
    public Disk(Disk disk) {
    	this.white = disk.isWhite();
    	this.isFreezed = disk.isFreezed;
    }
    
    /**
     * Kontrola jestli je kamen zamrzly.
     * @return - Vraci true v pripade, ze ano. Jinak false.
     */
    public boolean isFreezed() {
    	if (this.isFreezed)
    		return true;
    	else
    		return false;
    }
    
    /**
     * Necha kamen zmrznout.
     */
    public void freeze() {
    	this.isFreezed = true;
    }
    
    /**
     * Necha kamen rozmrznout.
     */
    public void unfreeze() {
    	this.isFreezed = false;
    }
    
    /**
     * Otoceni (zmena barvy) kamene.
     */
    public void turn() {
       if (this.isWhite())
           this.white = false;
       else
           this.white = true;
    }
    
    /**
     * Test, zda je kamen bily.
     * @return - Vraci true, pokud je kamen bily.
     */
    public boolean isWhite() {
       if (this.white)
           return true;
       else
           return false;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Disk other = (Disk) obj;
        if (!Objects.equals(this.white, other.white)) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 37 * hash + Objects.hashCode(this);
        return hash;
    }
    
    
}

/*
 * Autori: Milos Dolinsky, xdolin01
 *         Pavel Mencner, xmencn00
 * IJA 2015/2016
 * Rozhrani Field reprezentujici pole na hraci desce.
 */
package board;

/**
 * 
 * @author Pavel Mencner, Milos Dolinsky
 * Rozhrani Field reprezentujici pole na hraci desce.
 *
 */
public interface Field {
    
    /**
     * Vyctovy typ reprezentujici okoli (smery) jednotlivych poli.
     */
    public static enum Direction {
        D, L, LD, LU, R, RD, RU, U
    }
    
    /**
     * Prida sousedni pole field v danem smeru dirs.
     * @param dirs - Smer ve kterem se pridava pole.
     * @param field - Pridavane pole.
     */
    void addNextField(Field.Direction dirs, Field field);
    
    /**
     * Vraci kamen, ktery je vlozen na pole.
     * @return - Vlozeny kamen. Pokud je pole prazdne, vraci null.
     */
    Disk getDisk();
    
    /**
     * Vrati sousedni pole v danem smeru dirs.
     * @param dirs - Smer ve kterem se pridava pole.
     * @param board - Hraci deska.
     * @return - Sousedni pole v danem smeru dirs.
     */
    Field nextField(Field.Direction dirs, Board board);
    
    /**
     * Vlozi na pole kamen. Jednou vlozeny kamen jiz nelze odebrat.
     * @param disk - Vkladany kamen.
     * @return - Vraci uspesnost akce. Pokud je pole jiz obsazeno, vraci false.
     */
    boolean putDisk(Disk disk);
    
    /**
     * Test, zda je mozne vlozit na pole kamen. Pri testu se overuje soulad s pravidly hry.
     * @param disk - Vkladany kamen.
     * @return - Vraci uspesnost akce.
     */
    boolean canPutDisk(Disk disk);
    
    /**
     * 
     * @return
     */
    boolean isEmpty();
}

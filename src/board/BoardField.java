/*
 * Autori: Milos Dolinsky, xdolin01
 *         Pavel Mencner, xmencn00
 * IJA 2015/2016
 * Trida BoardField implementujici rozhrani Field. Reprezentuje aktivni policka na hraci desce.
 */
package board;

import java.util.Objects;

/**
 * 
 * @author Pavel Mencner, Milos Dolinsky
 * Vnitrni pole hraci desky.
 *
 */
public class BoardField extends java.lang.Object implements Field {
    private int row;
    private int col;
    private Disk D;
    private Field[] surr;
    
    
    /**
     * Inicializuje pole. Pole zna informaci o svem umisteni.
     * @param row - Radek, na kterem je pole umisteno.
     * @param col - Sloupec, na kterem je pole umisteno. 
     */
    public BoardField(int row, int col) {
        this.row = row;
        this.col = col;
        this.D = null;
        this.surr = new Field[8];
    }
    
    /**
     * Prida sousedni pole field v danem smeru dirs.
     * @param dirs - Smer ve kterem se pridava pole.
     * @param field - Pridavane pole. 
     */
    @Override
    public void addNextField(Field.Direction dirs, Field field) {
       switch (dirs) {
            case U:
                this.surr[1] = field;
                break;
            case D:
                this.surr[6] = field;
                break;
            case L:
                this.surr[3] = field;
                break;
            case R:
                this.surr[4] = field;
                break;
            case LU:
                this.surr[0] = field;
                break;
            case LD:
                this.surr[5] = field;
                break;
            case RU:
                this.surr[2] = field;
                break;
            case RD:
                this.surr[7] = field;
                break;
            default:
                break;
                
        } 
    }
    
    /**
     * Vrati sousedni pole v danem smeru dirs.
     * @param dirs - Smer ve kterem se nachazi pole.
     * @param board - Hraci deska
     * @return - Sousedni pole v danem smeru dirs.
     */
    @Override
    public Field nextField(Field.Direction dirs, Board board) {
        switch (dirs) {
            case U:
                return board.field[this.row - 1][this.col];
            case D:
                return board.field[this.row + 1][this.col];
            case L:
                return board.field[this.row][this.col - 1];
            case R:
                return board.field[this.row][this.col + 1];
            case LU:
                return board.field[this.row - 1][this.col - 1];
            case LD:
                return board.field[this.row + 1][this.col - 1];
            case RU:
                return board.field[this.row - 1][this.col + 1];
            default: //RD
                return board.field[this.row + 1][this.col + 1];
        }
    }
    
    /**
     * Vlozi na pole kamen. Jednou vlozeny kamen jiz nelze odebrat.
     * @param disk - Vkladany kamen.
     * @return - Vraci uspesnost akce. Pokud je pole jiz obsazeno, vraci false.
     */
    @Override
    public boolean putDisk(Disk disk) {
        if (disk == null)
        	return false;
        
    	if (this.D != null)
            return false;
        else {
            this.D = disk;
            return true;
        }
    }
    
    /**
     * Vraci kamen, ktery je vlozen na pole.
     * @return - Vlozeny kamen. Pokud je pole prazdne, vraci null.
     */
    @Override
    public Disk getDisk() {
        if (this.D == null)
            return null;
        else {
            return D;
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final BoardField other = (BoardField) obj;
        if (!Objects.equals(this.row, other.row) || !Objects.equals(this.col, other.col)) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 37 * hash + Objects.hashCode(this);
        return hash;
    }

    @Override
    public boolean canPutDisk(Disk disk) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public boolean isEmpty() {
        throw new UnsupportedOperationException("Not supported yet.");
    }
       
}

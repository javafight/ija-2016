/*
 * Autori: Milos Dolinsky, xdolin01
 *         Pavel Mencner, xmencn00
 * IJA 2015/2016
 * Trida Board reprezentujici hraci desku.
 */
package board;

/**
 * 
 * @author Pavel Mencner, Milos Dolinsky
 * Hraci deska a operace nad ni.
 *
 */
public class Board {
    private int size;
    protected Field[][] field;
    private Rules rule;
    
    /**
     * Inicializuje desku. Vytvori a ulozi si vsechna pole.
     * Pro vsechna pole nastavi jejich okoli.
     * Na neaktivnich polich jsou umisteny instance tridy BorderField.
     * Objekty aktivnich poli jsou ziskany metodou Rules.createField(int, int).
     * @param rules - Objekt reprezentujici pravidla inicializace hry.
     */
    public Board(Rules rules) {
        this.size = rules.getSize();
        this.field = new Field[size+2][size+2];
        this.rule = rules;
        
        for (int c = 0; c <= size + 1; c++) {
            for (int r = 0; r <= size + 1; r++) {
                if (r > 0 && r <= size && c > 0 && c <= size)
                	this.field[r][c] = rules.createField(r, c);
                else
                	this.field[r][c] = new BorderField();
            }
        }
    }
    
    /**
     * Vytvori kopii hraci desky predane jako parametr
     * @param board hraci deska
     */
    public Board(Board board) {
    	setBoard(board);
    }
    
    /**
     * Nastavi vlastnosti desky shodne s predanou deskou
     * @param board deska
     */
    public void setBoard(Board board) {
        this.size = board.getSize();
        this.field = new Field[size+2][size+2];
        this.rule = board.getRules();
        
        for (int c = 0; c <= size + 1; c++) {
            for (int r = 0; r <= size + 1; r++) {
                if (r > 0 && r <= size && c > 0 && c <= size) {
                	this.field[r][c] = board.getRules().createField(r, c);
                	
                	Disk d = board.getField(r, c).getDisk();
                	
                	if (d != null)
                		this.field[r][c].putDisk(new Disk(d));
                } else {
                	this.field[r][c] = new BorderField();
                }
            }
        }
    }
    
    /**
     * Vrati pole umistene na uvedenych indexech row a col.
     * @param row - Radek.
     * @param col - Sloupec.
     * @return - Pole na pozici [row][col].
     */
    public Field getField(int row, int col) {
       return this.field[row][col]; 
    }
    
    /**
     * Vraci velikost (rozmer) desky.
     * @return - Velikost desky.
     */
    public int getSize() {
        return this.size;
    }
    
    /**
     * Vraci objekt pravidel.
     * @return - Pravidla inicializace hry.
     */
   public Rules getRules() {
        return this.rule;
    }
}

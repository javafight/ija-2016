/*
 * Autori: Milos Dolinsky, xdolin01
 *         Pavel Mencner, xmencn00
 * IJA 2015/2016
 * Trida BorderField implementujici rozhrani Field. Reprezentuje okrajova policka na hraci desce. */
package board;

/**
 * 
 * @author Pavel Mencner, Milos Dolinsky
 * Okrajove pole hraci desky.
 *
 */
public class BorderField extends java.lang.Object implements Field {
    /**
     * Konstruktor. Nedela nic.
     */
    public BorderField() {
        // nothing
    }
    
    /**
     * Nedela nic.
     * @param dirs - Smer ve kterem se pridava pole.
     * @param field - Pridava pole.
     */
    @Override
    public void addNextField(Field.Direction dirs, Field field) {
        // nothing
    }
    
    /**
     * Nedela nic.
     * @param dirs - Smer ve kterem se pridava pole.
     * @param board - Hraci deska.
     * @return - Vzdy vraci null.
     */
    @Override
    public Field nextField(Field.Direction dirs, Board board) {
        return null;
    }
    
    /**
     * Nedela nic.
     * @param disk - Vkladany kamen.
     * @return - Vzdy vrati false.
     */
    @Override
    public boolean putDisk(Disk disk) {
        return false;
    }
    
    /**
     * Nedela nic.
     * @return - Vzdy vrati null.
     */
    @Override
    public Disk getDisk() {
        return null;
    }
    
    /**
     * Nedela nic.
     * @return - Vraci false.
     */
    @Override
    public boolean isEmpty(){
        return false;
    }

    /**
     * Nedela nic.
     * @param disk - Vkladany kamen.
     * @return - Vraci false.
     */
    @Override
    public boolean canPutDisk(Disk disk) {
        return false;
    }
}

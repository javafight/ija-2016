/*
 * Autori: Milos Dolinsky, xdolin01
 *         Pavel Mencner, xmencn00
 * IJA 2015/2016
 * Rozhrani Rules reprezentujici souhrn pravidel.
 */
package board;

/**
 *
 * @author Milos Dolinsky, Pavel Mencner
 * Rozhrani Rules reprezentujici souhrn pravidel.
 * 
 */
public interface Rules {
    /**
     * Vraci velikost desky.
     * @return - Velikost desky.
     */
    int getSize();
    
    /**
     * Vraci pocet kamenu jednotlivych hracu.
     * @return - Pocet kamenu.
     */
    int numberDisks();
    
    /**
     * Vytvori odpovidajici pole na zadanych indexech.
     * @param row - Radek desky.
     * @param col - Sloupec desky.
     * @return - Vytvorene pole.
     */
    Field createField(int row, int col);
    
    /**
     * Vraci jestli je aktivovano zamrzani kamenu.
     * @return - True jestli je aktivovano. Jinak false.
     */
    public boolean isFreezing();
    
    public int getGaps();
    
    public int getMaxTime();
    
    public int getNumDisks();
}
